#include "Scanner.h"
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

const int count_kw = 15;
const int count_type_name = 4;
const std::string kw[count_kw] = {"for", "do", "while", "return", "typedef", "enum", "break", "struct", "if", "else", "continue", "const", "printf", "scanf", "class"};
const std::string TYPE_NAME[count_type_name] = { "void", "int", "char", "float"};

static bool isLetter(char c)
{
	return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_';
}

static bool isDigit(char c)
{
	return c >= '0' && c <= '9';
}

static bool isWhitespace(char c)
{
	return c == 9 || c == 10 || c == 32; 
}

static bool isOperation(char c)
{
	return c == '+' || c == '-' || c == '<' || c == '>' || c == '/' || c == '*' || c == '=' || c == '|' || c == '&'  || c == '!' || c== '%' || c== '^' || c== '~';
}

static bool isSeparator(char c)
{
	return c == ':' || c == ';' || c == '(' || c == ')' || c == '[' || c == ']' || c == ',' || c == '.' || c == '{' || c == '}';
}

static std::string getValueOfString (std::string text)
{
	std::string value = "";
	text = text.substr(1, text.size() - 2);
	for (int i = 0; i < text.size(); i++)
	{
		if (text[i] == '\\')
		{
			switch (text[i + 1])
			{
				case 'n' :
					text[i + 1] = '\n';
					break;
				case 't' :
					text[i + 1] = '\t';
					break;
			}
		} else {
			value += text[i];
		}
	}
	return value;
}

static std::string HexToDec (std::string text)
{
	int value = 0;
	int buf;
	
	text = text.substr(2, text.size() - 2);
	for (int i = text.size() - 1 ; i >= 0; i--)
	{
		if (text[i] >= 'A' && text[i] <= 'F')
		{
			buf = 10 + text[i] - 'A';
		} else if (text[i] >= 'a' && text[i] <= 'f'){
			buf = 10 + text[i] - 'a';
		} else {
			buf = text[i] - '0';
		}
		value += buf * pow(16, text.size() - i - 1);
	}
	std::stringstream ss;
	ss << value;
	return ss.str();
}

static bool valueInArray(std::string val, const std::string ar[], int size){
	bool resalt = false;
	for (int i = 0; i < size; i++)
		resalt |= val == ar[i];
	return resalt;
}

bool Scanner::Next()
{
	bool stop = false;
	char ch = buffer != "" ? buffer[0] : fgetc(file);
	do {
		switch(cur_state)
		{
			case whitespace :
				if (isLetter(ch)){
					cur_state = identifier;				
				} else if (ch == '\"') {
					cur_state = string;
				} else if (ch == '\'') {
					cur_state = char_;
				} else if (isDigit(ch)) {
					cur_state = number_int;
				} else if (isWhitespace(ch)){
					if (ch == 10) {         // \n       
						cur_line++;
						cur_column = 1;
					} else if (ch == 9) {   // tab
						cur_column += 4;
					} else if (ch == 32) {  // spa�e
						cur_column++;
					}
				} else if (isOperation(ch) || (ch == '?')) {
					cur_state = operation;
				} else if (isSeparator(ch)) {
					cur_state = separator;
				} else if (ch == EOF) {
					last_tokens = NULL;
					return false;
				} else {
					cur_state = error;
				}
				buffer = ch;
				break;
				
			case identifier :
				if (isLetter(ch) || isDigit(ch)) {
					cur_state = identifier;
					buffer += ch;
				} else {
					if (valueInArray(buffer, TYPE_NAME, count_type_name)) {
						last_tokens = new Type(cur_line, cur_column, buffer, buffer);
					} else if (valueInArray(buffer, kw, count_kw)) {
						last_tokens = new KeyWords(cur_line, cur_column, buffer, buffer);
					} else {
						last_tokens = new Identifier(cur_line, cur_column, buffer, buffer);
					}
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace;
				} break;

			case string:
				if ((ch == '\"') && (buffer[buffer.size() -1] != '\\')) {
					last_tokens = new String(cur_line, cur_column, buffer + ch, getValueOfString(buffer + ch));
					cur_column += buffer.size();
					buffer = "";	
					stop = true;
					cur_state = whitespace;
				} else if ((ch == 10) || (ch == EOF)) {
					last_tokens = new Error(cur_line, cur_column, buffer, buffer);
					stop = true;
					buffer = "";	
					cur_state = whitespace;
				} else {
					buffer += ch;
				} break;

			case char_:
				if (ch == '\'') {
					if (buffer.size() == 1 || buffer.size() > 3 || (buffer.size() == 3 && buffer[1] != '\\')) {
						cur_state = error;
						buffer += ch;
					} else if (buffer.size()  == 2 && buffer[1] == '\\') {
						buffer += ch;
					} else if (ch == EOF) {
						last_tokens = new Error(cur_line, cur_column, buffer + ch, buffer + ch);
						cur_state = whitespace;
					} else {
						last_tokens = new Char(cur_line, cur_column, buffer + ch, getValueOfString(buffer + ch));
						cur_column += buffer.size();
						buffer = "";	
						stop = true;
						cur_state = whitespace;
					}
				} else if (ch == 10) {
					cur_state = error;
					buffer += ch;
				} else {
					buffer += ch;
				} break;

			case number_int:
				if (isDigit(ch)) {
					buffer += ch;
				} else if ((buffer + ch) == "0x") {
					cur_state = number_hex;
					buffer += ch;
				} else if (ch == '.') {
					cur_state = number_float;
					buffer += ch;
				} else if (isWhitespace(ch) || isOperation(ch) || (isSeparator(ch) && (ch != '(')) || ch == EOF) {
					last_tokens = new Number(cur_line, cur_column, buffer, buffer);
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace;
				} else if (ch == EOF) {
					last_tokens = new Error(cur_line, cur_column, buffer, buffer);
					cur_state = whitespace;
				} else {
					cur_state = error;
					buffer += ch;
				} break;

			case number_float:
				if (isDigit(ch)) {
					buffer += ch;
				} else if (ch == 'e') {
					cur_state = number_exp;
					buffer += ch;
				} else if ((isWhitespace(ch) || isOperation(ch) || (ch == EOF) 
					|| (isSeparator(ch) && (ch != '(') && (ch != '.'))) 
					&& (buffer[buffer.size() - 1] != '.')) {
					last_tokens = new NumberFloat(cur_line, cur_column, buffer, buffer);
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace; 
				} else if (ch == EOF) {
					last_tokens = new Error(cur_line, cur_column, buffer, buffer);
					cur_state = whitespace;
					throw "Syntax error in line " + std::to_string(cur_line) + ", column " + std::to_string(cur_column);
				} else {
					cur_state = error;
					buffer += ch;
				} break;

			case number_hex:
				if (isDigit(ch) || ch >= 'A' && ch <= 'F' || ch >= 'a' && ch <= 'f') {
					buffer += ch;
				} else if (isWhitespace(ch) || isOperation(ch) || isSeparator(ch) || ch == EOF) {
					last_tokens = new Number(cur_line, cur_column, buffer, HexToDec(buffer));
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace; 
				} else {
					cur_state = error;
					buffer += ch;
				} break;

			case number_exp:
				if (buffer[buffer.size() - 1] == 'e' && (ch == '+' || ch == '-')) {
					buffer += ch;
				} else if ((buffer[buffer.size() - 1] == '-' || buffer[buffer.size() - 1] == '+') && isDigit(ch)) {
					buffer += ch; 
				} else if (isDigit(buffer[buffer.size() - 1]) && isDigit(ch)) {
					buffer += ch;
				}else if (isWhitespace(ch) || isOperation(ch) || isSeparator(ch) || ch == EOF) {
					last_tokens = new NumberFloat(cur_line, cur_column, buffer, buffer);
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace; 
				} else {
					cur_state = error;
					buffer += ch;
				} break;

			case operation:
				if (isLetter(ch) || isDigit(ch) || isWhitespace(ch) || isSeparator(ch) 
					|| ((buffer + ch) != "/*" && ch == '*') || ch == '\'' || ch == '"'
					|| (ch != '=' && (buffer == "!" || buffer == "~" || buffer == "*"))) {
					last_tokens = new Operation(cur_line, cur_column, buffer, buffer);
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace;
				} else if (((ch == '=') && (buffer != "?")) 
					|| (isOperation(ch) && (buffer[0] == ch) && (ch != '*') && (ch != '/') && (ch != '%'))
					|| (buffer + ch) == "->"){              
					last_tokens = new Operation(cur_line, cur_column, buffer + ch, buffer + ch);
					cur_column += buffer.size();
					buffer = "";
					stop = true;
					cur_state = whitespace;
				} else if ((buffer + ch) == "//") {
					buffer += ch;
					cur_state = comment;
				} else if ((buffer + ch) == "/*") {
					buffer += ch;
					cur_state = complex�omment;
				} else if (ch == EOF) {
					last_tokens = new Error(cur_line, cur_column, buffer, buffer);
					cur_state = whitespace;
					throw "Syntax error in line " + std::to_string(cur_line) + ", column " + std::to_string(cur_column);
				} else {
					cur_state = error;
					buffer += ch;
				}
				break;

			case comment:
				if ((ch == 10) || (ch == EOF)){
					cur_column += buffer.size();
					buffer = ch;
					cur_state = whitespace; 
				} else {
					buffer += ch;
				} break;
			
			case complex�omment:
				if (buffer.size() > 2 && buffer[buffer.size() - 1] == '*' && ch == '/' ) {
					cur_column += buffer.size();
					buffer = "";
					last_tokens = NULL;
					cur_state = whitespace; 
				} else if (ch == EOF) {
					last_tokens = new Error(cur_line, cur_column, buffer, buffer);
					cur_state = whitespace;
					throw "Syntax error in line " + std::to_string(cur_line) + ", column " + std::to_string(cur_column);
				} else {
					buffer += ch;
				} break;

			case separator:
				if (buffer == "." && isDigit(ch)) {
					cur_state = number_float;
					buffer += ch;
				} else {
					last_tokens = new Separator(cur_line, cur_column, buffer, buffer);
					cur_column += buffer.size();
					buffer = ch;
					stop = true;
					cur_state = whitespace;
				} break;

			case error:
				while (!isWhitespace(ch) && (ch != EOF)) {
					buffer += ch;
					ch = fgetc(file);
				}
				last_tokens = new Error(cur_line, cur_column, buffer, buffer);
				cur_column += buffer.size();
				buffer = ch;
				stop = true;
				cur_state = whitespace;
				throw "Syntax error in line " + std::to_string(last_tokens->Line()) + ", column " + std::to_string(last_tokens->Column());
				break;
		}
	} while (!stop && (((ch = fgetc(file)) != EOF) || cur_state != whitespace));
	return ch != EOF;
}

bool Token::operator == (const Token* token) const
{
	return (this == NULL && token == NULL) || (token && token->type == this->type && token->text == this->text);
}

bool Token::operator != (const Token* token) const
{
	return  (this == NULL && token != NULL) || (this != NULL && token == NULL) || token->type != this->type || token->text != this->text;
}
