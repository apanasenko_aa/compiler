#include "Scanner.h"
#include "Pars.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>


void PrintHelp()
{
	std::cout << "My compiler";
}

std::string MASM_Addres;

int main(int argc, char* argv[])
{	
	if (argc == 1) {
		PrintHelp();
	} else {
		std::vector<Token* > tokens;
		std::string s = argv[0];
		FILE* inFile = fopen(argv[2], "r");
		MASM_Addres = s.substr(0, s.rfind("\\") + 1) + "..";
		std::string ml_exe = s.substr(0, s.rfind("\\") + 1) + "..\\masm32\\bin\\ml.exe /c /coff ";
		std::string link_exe = s.substr(0, s.rfind("\\") + 1) + "..\\masm32\\bin\\link.exe /SUBSYSTEM:CONSOLE /OUT:";
		std::string out_asm = "temp";
		std::string asm_ = ".asm ";
		std::string exe_ = ".exe ";
		std::string obj_ = ".obj ";
		try {
			if (inFile) {
				Scanner scanner(inFile);
				while (scanner.Next()) {
					tokens.push_back(scanner.Get());
				}
				if (scanner.Get() != NULL) {
					tokens.push_back(scanner.Get());
				}
			} else {
				std::cout << "No file in directory" << std::endl;
			}
			if (argv[1][0] == 'l') {
				FILE* outFile = fopen(argv[3], "w");
				std::fstream outStream(outFile);
				if (tokens.size() == 0) {
					outStream << "Empty file" << std::endl;
				}
				for (int i = 0; i < tokens.size(); i++) {
					Token* t = tokens[i];
					outStream << t->ClassName() << '\t' 
					          << t->Line() << '\t' 
							  << t->Column() << '\t' 
							  << t->GetText() << "\t" 
							  << t->GetValue() << std::endl;
				}
				outStream.close();
				fclose(outFile);
			} 
			else if (argv[1][0] == 'p')
			{
				FILE* outFile = fopen(argv[3], "w");
				Pars simplePars(tokens);
				simplePars.GetExpression()->Print("", outFile);
				fclose(outFile);
			} 
			else if (argv[1][0] == 't')
			{
				FILE* outFile = fopen(argv[3], "w");
				Pars simplePars(tokens);
				simplePars.GetSymbolTable()->Print("", outFile);
				fclose(outFile);
			}
			else if (argv[1][0] == 'o')
			{
				FILE* outFile = fopen(argv[3], "w");
				Pars simplePars(tokens);
				simplePars.GetSymbolTable()->Optimize()->Print("", outFile);
				fclose(outFile);
			}
			else if (argv[1][0] == 'g')
			{
				FILE* asm_file = fopen((out_asm + asm_).c_str(), "w");
				//Pars simplePars(tokens);
				//simplePars.GetSymbolTable()->Optimize()->GlobalGenerate(&CodeGenerator(asm_file));
				Pars simplePars(tokens, &CodeGenerator(asm_file));
				simplePars.generateCode();
				fclose(asm_file);
				std::cout << ml_exe + out_asm + asm_ << std::endl;
				system((ml_exe + out_asm + asm_).c_str());
				std::cout << link_exe + out_asm + exe_ + out_asm + obj_ << std::endl;
				system((link_exe + out_asm + exe_ + out_asm + obj_).c_str());
				system((out_asm + exe_ + " > " + argv[3]).c_str());
				system((out_asm + exe_).c_str());
				std::cout << std::endl;
				
			}
		} catch (std::string ERROR) {
			FILE* outFile = fopen(argv[3], "w");
			fprintf(outFile, "%s", ERROR.c_str());
			fclose(outFile);
			//std::cout << ERROR << std::endl;
		}		
		fclose(inFile);
	}
	return 0; 
}