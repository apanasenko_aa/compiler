#include "Pars.h"
#include "Const_operation.h"


Symbol* GetDecSymbol(Block*block, std::string name)
{
	if (!block) return nullptr;
	return block->tags.count(name) && block->tags[name] < block->symbols.size()
		? block->symbols[block->tags[name]]
		: GetDecSymbol(block->parent, name);
};


static int GetPrecedencBinOper(const Token* t) 
{
	for (int i = 0; i < COUNT_LEVELS_BO; i++)
		for (int j = 0; j < COUNT_TOKEN_IN_LEVEL_BO && PrecBinOp[i][j]; j++)
			if (*t == PrecBinOp[i][j])
				return i + 1;
	/* level 16 */
	return (*t == POINT || *t == ARROW) 
		? HIGHER_PRIORITY
		: (*t == FUNC || *t == ARRAY) 
			? HIGHER_PRIORITY + 1 
			: 0;
}


void Pars::generateCode()
{
	Block* block = GetSymbolTable();
	generator->generate();
}


SymbolType* Pars::GetTypeIdentByName(Block* block,  std::string name)
{
	if (!block) return VOID;
	if (block->tags.count(name))
	{
		Symbol* s = block->symbols[block->tags[name]];
		switch(s->s_type)
		{
			case _name_ :
				return dynamic_cast<SymbolWithType*>(s)->type;
			case _func_ :
				return VOID;
			case _typedef_:
			case _struct_:
				throw_if(true, "Unexpected identification");
		}
	}
	else 
	{
		return GetTypeIdentByName(block->parent, name);
	}
}


static int GetPrecedencUnOper(Token* t) 
{
	for (int i = 0; i < COUNT_UO; i++)
		if (*t == PrecedencUnOp[i])
			return PRIORITY_UNARY_OP;
	return 0;
}


static int IsLeftAssOp (const Token* t) {
	for (int i = 0; i < COUNT_LAO; i++)
		if (*t == leftAssociativeOp[i])
			return 1;
	return 0;
}


void Pars::throw_if(bool condition, std::string message)
{
	if (condition)
		throw GetToken()->GetPosition() + " Error : " + message;
}


bool Pars::isType(Token* token, Block* block)
{
	bool result = token->GetType() == type_name;
	return result 
		|| IsNameDeclaration(token->GetValue(), block, true, _typedef_) 
		|| IsNameDeclaration("$_" + token->GetValue(), block, true, _class_);
}


ExpNode* Pars::GetExpression()
{
	return ParsExpression(NULL, SEMICOLON);
}


Block* Pars::GetSymbolTable()
{
	return ParsBlock(NULL, NULL);
}


bool Pars::IsNameDeclaration(std::string name
							,Block* block
							,bool search_in_parent
							,symbol_type WhatWeAreLookingFor
							,bool HaveFuncBody
							,bool inExp )
{
	if (!block) return false;
	bool result = false;
	for (int i = 0; i < block->symbols.size() && !result; i++) {
		result |= block->symbols[i]->s_type == WhatWeAreLookingFor
			&& block->symbols[i]->name == name
			&& (block->symbols[i]->s_type != _func_
			|| inExp
			|| (!dynamic_cast<SymbolFunc*>(block->symbols[i])->body
			&& HaveFuncBody));
	}
	return result 
			|| search_in_parent 
			&& IsNameDeclaration(name, block->parent, search_in_parent, WhatWeAreLookingFor, HaveFuncBody, inExp)
			|| WhatWeAreLookingFor == _name_ 
			   && block->haveParams 
			   && IsNameDeclaration(name, block->parent, false, _name_);
}


Block* Pars::ParsFuncParams(Block* parent)
{
	Block* block = new Block(parent);
	do 
	{
		NextToken();
		if (*GetToken() == CLOSE_BRACKET) break;
		bool isConst = *GetToken() == CONST;
		cur_position += isConst;
		if (*GetToken() == STRUCT)
		{
			NextToken();
			block->add(ParsStruct(block, isConst, true));
		}
		else 
		{
			throw_if(!isType(GetToken(), parent), "Expect type name");
			if (IsNameDeclaration("$_" + GetToken()->GetValue(), block, true, _class_)) 
				block->add(ParsSymbol(block, NULL, new SymbolScalar("$_" + NextToken()->GetValue(), isConst), true));
			else
				block->add(ParsSymbol(block, NULL, new SymbolScalar(NextToken()->GetValue(), isConst), true));
		}
	}
	while (*GetToken() == COMMA);
	throw_if(*NextToken() != CLOSE_BRACKET, "Expect ')'");
	return block;
}


SymbolWithType* Pars::ParsArray(SymbolWithType* basis_type, const Token* ExitCondition)
{
	if (!basis_type 
		|| *GetToken() != OPEN_SQ_BRACKET
		|| *GetToken() == ExitCondition)
		return basis_type;
	int size = tokens[++cur_position]->GetType() == number 
			 ? std::stoi(NextToken()->GetValue())
			 : 0;
	throw_if(*NextToken() != CLOSE_SQ_BRACKET, "Expect ']'");
	basis_type = ParsArray(basis_type, ExitCondition);
	basis_type->type = new SymbolArray(basis_type->type, size);
	return basis_type;
}


SymbolWithType* Pars::ParsSymbol(Block* parent
							    ,const Token* ExitCondition
								,SymbolType* basis_type
								,bool isFuncPapams
								,bool mustReturn)
{
	SymbolWithType* result = NULL;
	Token token  = *NextToken();
	if (token == POINTER)
	{
		result = ParsSymbol(parent, NULL, basis_type, false, true);
		result->type = new SymbolPoint(result->type);
	} 
	else if (token.GetType() == identifier)  
	{
		result = new SymbolValue(basis_type, token.GetValue());
	} 
	else if (!result && token == OPEN_BRACKET)
	{
		result = ParsSymbol(parent, CLOSE_BRACKET, basis_type);
		throw_if(*NextToken() != CLOSE_BRACKET, "Expect ')'");	
	}
	if (mustReturn)
		return result;
	result = ParsArray(result, ExitCondition);
	if (isFuncPapams || *GetToken() == ExitCondition)
	{
		throw_if(!result, "in params declaration");	
		throw_if(IsNameDeclaration(result->name, parent, false), 
					"Redefinition of '" +  result->name + "'");
		return result;
	}
	if (result && *GetToken() == OPEN_BRACKET)
	{
		Block* params = ParsFuncParams(parent);
		Block* body = NULL;
		if (*GetToken() == OPEN_BLOCK) 
		{
			SymbolType* thisBlockType = curBlockType;
			curBlockType = result->type;
			NextToken();
			body = ParsBlock(params, CLOSE_BLOCK, true);
			throw_if(*NextToken() != CLOSE_BLOCK, "Expect '}'");
			curBlockType = thisBlockType;
		}
		throw_if(IsNameDeclaration(result->name, parent, false)
				  && !IsNameDeclaration (result->name, parent,  false, _func_, body), 
				  "Redefinition of '" + result->name + "'");
		SymbolFunc* result_f = new SymbolFunc(result->type, result->name, params, body);
		delete result;
		return result_f;
	} 
	else if (dynamic_cast<SymbolValue*>(result) && *GetToken() == ASSIGN)
	{
		NextToken();
		SymbolValue* temp = dynamic_cast<SymbolValue*>(result);
		temp->value = ParsExpression(parent, SEMICOLON);//////////////////////////////////////////////////////////////////
		throw_if(!temp->type->CanConvertTo(temp->value->type), "initialized with the wrong type");
	}
	throw_if(IsNameDeclaration( result->name, parent, false), 
				"Redefinition of '" +  result->name + "'");
	throw_if(!result, "Can't declaration this bullshit");
	if (genCode)
		result->Generate(generator->data, generator->code);
	return result;
}


void Pars::ParsDeclaration(Block* block, Block* parent)
{
	bool isConst = *GetToken() == CONST;
	cur_position += isConst;
	if (*GetToken() == STRUCT)
	{
		NextToken();
		block->add(ParsStruct(block, isConst));
	}
	else 
	{
		throw_if(!isType(GetToken(), block), "Expect type name");
		std::string nameType = NextToken()->GetValue();
		if (IsNameDeclaration("$_" + nameType, block, true, _class_)){
			nameType = "$_" + nameType;
		}
		SymbolType* basis_type = new SymbolScalar(nameType, isConst);
		do 
		{
			block->add(ParsSymbol(block, NULL, basis_type));
		} 
		while (*NextToken() == COMMA);
		cur_position--;
		throw_if(*GetToken() != SEMICOLON, "Expect ';'");
	}
}


Symbol* Pars::ParsStruct(Block* parent, bool isConst, bool isFuncPapams)
{
	Block* struct_block = NULL;
	Symbol* result = NULL;
	throw_if(GetToken()->GetType() != identifier,"Expect identifier");
	std::string nStruct = NextToken()->GetText();	
	if (*GetToken() == OPEN_BLOCK) 
	{
			NextToken();
			struct_block = ParsBlock(parent, CLOSE_BLOCK);
			throw_if(*NextToken() != CLOSE_BLOCK, "Expect '}'");
			result = new SymbolStruct(nStruct, struct_block);
	}	
	throw_if(struct_block && IsNameDeclaration(nStruct, parent, false, _struct_),
				"Redefinition struct of '" + nStruct + "'");
	throw_if(!struct_block && !IsNameDeclaration(nStruct, parent, true, _struct_),
				"Struct '" + nStruct + "' not declaration");
	throw_if(struct_block && isFuncPapams, 
				"Can't declaration struct in function params");
	do 
	{
		if (*GetToken() == SEMICOLON)
			return result;	
		if (result)
			parent->add(result);			
		result = ParsSymbol(parent, NULL, new SymbolScalar("$_" + nStruct, isConst), isFuncPapams); 
	} 
	while (*NextToken() == COMMA && !isFuncPapams);
	cur_position--;
	return result;
};


ExpNode* Pars::ParsCondition(Block* parent)
{
	throw_if(*NextToken() != OPEN_BRACKET, "Expect '('");
	ExpNode* block = ParsExpression(parent, CLOSE_BRACKET);
	throw_if(*block->type == VOID, "Cannot void in 'if' condition");
	throw_if(*NextToken() != CLOSE_BRACKET, "Expect ')'");
	return block;
}


Statement* Pars::ParsStatement(Block* parent)
{
	Statement* result = NULL;
	if (*GetToken() == IF)
	{
		NextToken();
		ExpNode* condition = ParsCondition(parent);
		Statement* block_true = ParsStatement(parent);
		Statement* block_false = NULL;
		if (*GetToken() == ELSE)
		{
			NextToken();
			block_false = ParsStatement(parent);
		}
		result = new Node_if(condition, block_true, block_false);
	} 
	else if (*GetToken() == FOR) 
	{
		NextToken();
		throw_if(*NextToken() != OPEN_BRACKET, "Expect '('");
		ExpNode* start = ParsExpression(parent, SEMICOLON);
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
		ExpNode* end = ParsExpression(parent, SEMICOLON);
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
		ExpNode* step = ParsExpression(parent, CLOSE_BRACKET);
		throw_if(*NextToken() != CLOSE_BRACKET, "Expect ')'");
		result = new Node_for(start, end, step, ParsStatement(parent));
	}
	else if (*GetToken() == WHILE) 
	{
		NextToken();
		ExpNode* end = ParsCondition(parent);
		result = new Node_while(end, ParsStatement(parent));
	}
	else if (*GetToken() == DO) 
	{
		NextToken();
		Statement* loop = ParsStatement(parent);
		throw_if(*NextToken() != WHILE, "Expect 'while'");
		result = new Node_do(ParsCondition(parent), loop);
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
	}
	else if (*GetToken() == PRINTF) 
	{
		NextToken();
		throw_if(*NextToken() != OPEN_BRACKET, "Expect '('");
		throw_if(GetToken()->GetType() != string, "Expect format string");
		std::string format = NextToken()->GetValue();
		result = new Node_printf(format);
		int pos = 0;
		while ((pos = format.find("%", pos)) != std::string::npos)
		{
			pos++;
			throw_if(*NextToken() != COMMA, "Expect ','");
			dynamic_cast<Node_printf*>(result)->add(ParsExpression(parent, 
				format.find("%", pos) != std::string::npos ? COMMA : CLOSE_BRACKET));
		}
		throw_if(*NextToken() != CLOSE_BRACKET, "Expect ')'");
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
		if (genCode)
			result->Generate(generator->data, generator->code);
	}
	else if (*GetToken() == BREAK) 
	{
		NextToken();
		result = new Node_break();
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
	}
	else if (*GetToken() == CONTINUE) 
	{
		NextToken();
		result = new Node_continue();
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
	}
	else if (*GetToken() == RETURN) 
	{
		NextToken();
		ExpNode* return_expression = ParsExpression(parent, SEMICOLON);
		result = new Node_return(return_expression);
		if (!(*curBlockType == VOID && !return_expression->type))
			throw_if(!curBlockType->CanConvertTo(return_expression->type), "Error type in return");
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
	} 
	else if(*GetToken() == OPEN_BLOCK)
	{
		NextToken();
		result = new Node_block(ParsBlock(parent, CLOSE_BLOCK));
		throw_if(*NextToken() != CLOSE_BLOCK, "Expect '}'");
	} 
	else 
	{
		result = new Node_block(ParsBlock(parent, SEMICOLON));
		throw_if(*NextToken() != SEMICOLON, "Expect ';'");
	}
	throw_if(!result, "Undefined Statement");
	return result;
}


SymbolTypedef* Pars::ParsTypedef(Block* parent)
{
	SymbolTypedef* result = NULL;
	NextToken();
	if (*GetToken() == STRUCT)
	{
		NextToken();
		Symbol* new_t = ParsStruct(parent, false);
		SymbolValue* new_name = dynamic_cast<SymbolValue*>(new_t);
		throw_if(!new_name, "Typedef can't find new name after struct");
		result = new SymbolTypedef(new_name->name, new_name->type);
		delete new_name;
	}
	else if (isType(GetToken(), parent))
	{
		SymbolType* basis_type = new SymbolScalar(NextToken()->GetValue(), false);
		SymbolWithType* new_t = ParsSymbol(parent, NULL, basis_type);
		SymbolValue* new_name = dynamic_cast<SymbolValue*>(new_t);
		throw_if(!new_name, "Typedef can't find new name after type name");
		result = new SymbolTypedef(new_t->name, new_t->type);
		delete new_name;			
	}
	else
		throw_if(true, "Undefined identifier after typedef");
	return result;	
}


SymbolClass* Pars::ParsClass(Block* parent)
{
	//throw_if(parent->parent, "You must declarate class in global");
	Block* class_block = new Block(parent);
	SymbolClass* parentClass = NULL;
	NextToken();
	throw_if(GetToken()->GetType() != identifier, "Expect identifier");
	std::string nClass = "$_" + NextToken()->GetText();

	throw_if(IsNameDeclaration(nClass, parent, true, _class_),
		"Redefinition class of '" + nClass + "'");
	
	if (*GetToken() == COLON){
		NextToken();
		throw_if(GetToken()->GetType() != identifier, "Expect identifier");
		std::string parentName = "$_" + NextToken()->GetText();
		throw_if(!parent->tags.count(parentName), "Name of parent class not declarate");
		parentClass = dynamic_cast<SymbolClass*> (parent->symbols[parent->tags[parentName]]);
		class_block->parent = parentClass->body;
	}
	
	throw_if(*GetToken() != OPEN_BLOCK, "Expect '{'");
	do{
		NextToken();
		if (*GetToken() == CLOSE_BLOCK) break;
		bool isConst = *GetToken() == CONST;
		if (isConst){
			NextToken();
		}
		if (*GetToken() == CLASS) {
			class_block->add(ParsClass(class_block));
			throw_if(*NextToken() != SEMICOLON, "Expect ;");
		}
		throw_if(!isType(GetToken(), class_block), "Expect type name");
		SymbolType* basis_type = new SymbolScalar(NextToken()->GetValue(), isConst);
		int count_var = 0;
		do {
			if (count_var) 
				NextToken();
			class_block->add(ParsSymbol(class_block, NULL, basis_type));
			count_var++;
		} while (*GetToken() == COMMA);
	} while (*GetToken() == SEMICOLON);
	throw_if(*NextToken() != CLOSE_BLOCK, "Expect '}'");

	return new SymbolClass(nClass, parentClass, class_block);
}


Block* Pars::ParsBlock(Block* parent, const Token* ExitCondition, bool HaveParams)
{
	Block* block = new Block(parent, HaveParams);
	while (cur_position < tokens.size() && *GetToken() != ExitCondition)
	{
		Token token  = *GetToken();
		if (token == TYPEDEF)
		{
			block->add(ParsTypedef(block));
		}
		else if (isType(GetToken(), block) || token == CONST || token == STRUCT)
		{
			ParsDeclaration(block, parent);
		}
		else if (token == CLASS)
		{
			block->add(ParsClass(block));
		}
		else if (token.GetType() == key_words || token == OPEN_BLOCK)
		{
			block->add(ParsStatement(block));
		} 
		else if (token == SEMICOLON)
		{
			NextToken();
		} 
		else 
		{
			ExpNode* node = ParsExpression(block, SEMICOLON); 
			block->add(new Node_expression(node));
			if (genCode)
				node->GenerateValue(generator->code);
		}
	}
	return block;
}


ExpNode* Pars::ParsExpression(Block* parent_, const Token* ExitCondition)
{
	Block* parent = parent_;
	std::vector<ExpNode*> nodes;
	std::vector<Token*> operations;
	std::vector<int> prece_oper;
	int count_un_op = 0;

	while (cur_position < tokens.size() && *GetToken() != ExitCondition) {
		
		if (nodes.size() == operations.size() - count_un_op) {

			while (GetPrecedencUnOper(GetToken())){
				operations.push_back(GetToken());
				prece_oper.push_back(GetPrecedencUnOper(GetToken()));
				count_un_op++;
				NextToken();
			}		
			Token_type tt = GetToken()->GetType();
			if (tt == number 
				|| tt == my_char
				|| tt == identifier 
				|| tt == string 
				|| tt == my_float) 
			{
				if (tt == identifier && operations.size() > 0 && *operations.back() == POINT){
					ValNode* lastNode = dynamic_cast<ValNode*>(nodes.back());
					throw_if(!lastNode, "Identifier '" + lastNode->value->GetText() + "' must be class");
					std::string nameClass = lastNode->type->toString();
					SymbolClass* sClass = dynamic_cast<SymbolClass*>(GetDecSymbol(parent, nameClass));
					throw_if(!sClass || 
						 (!IsNameDeclaration(GetToken()->GetValue(), sClass->body, true, _name_)
						&& !IsNameDeclaration(GetToken()->GetValue(), sClass->body, true, _func_, false, true)),
						"identifier '" + GetToken()->GetValue() + "' not declaration");
					parent = sClass->body;
					//operations.pop_back();
					//nodes.pop_back();
				}
				else {
					throw_if(tt == identifier
						&& !IsNameDeclaration(GetToken()->GetValue(), parent, true, _name_)
						&& !IsNameDeclaration(GetToken()->GetValue(), parent, true, _func_, false, true),
						"identifier '" + GetToken()->GetValue() + "' not declaration");
				}
				ValNode* node = NULL;
				switch (tt)
				{
					case number:
						node = new ValNode(GetToken(), INT);
						break;
					case my_float:
						node = new ValNode(GetToken(), FLOAT); 
						break;
					case string:
						node = new ValNode(GetToken(), STRING); 
						break;
					case my_char:
						node = new ValNode(GetToken(), CHAR); 
						break;
					case identifier:
						node = new ValNode(GetToken(), GetTypeIdentByName(parent, GetToken()->GetValue()));
						break;
				}
				nodes.push_back(node);
			} else if (*GetToken() == OPEN_BRACKET) {
				NextToken();
				nodes.push_back(ParsExpression(parent, CLOSE_BRACKET));
			} else {
				throw_if(true, "Expect identifier or number");
			}
			NextToken();	
		}

		if (cur_position < tokens.size() && (*GetToken() == INCREMENT || *GetToken() == DECREMENT)) {
			ExpNode* node = nodes.back();	
			nodes.pop_back();
			UnOpNode* new_node = new UnOpNode(GetToken(), node);
			CheckTypeInUnOpNode(new_node);
			nodes.push_back(new_node);			
			NextToken();
		}

		if (cur_position < tokens.size() && *GetToken() != ExitCondition 
			&& GetPrecedencBinOper(GetToken())) {
			while (operations.size() > 0 
				  && prece_oper.back() >= GetPrecedencBinOper(GetToken()) + IsLeftAssOp(GetToken())) {
				ExpNode* r = nodes.back();
				nodes.pop_back();
				if (prece_oper.back() == PRIORITY_UNARY_OP){
					UnOpNode* new_node = new UnOpNode(operations.back(), r);
					CheckTypeInUnOpNode(new_node);
					nodes.push_back(new_node);					
					count_un_op--;
				} else {
					ExpNode* l = nodes.back();
					nodes.pop_back();
					BinOpNode* new_node = new BinOpNode(operations.back(), r, l);
					CheckTypeInBinOpNode(new_node, parent);
					nodes.push_back(new_node);
				}
				operations.pop_back();
				prece_oper.pop_back();
			}
			if (*GetToken() == OPEN_BRACKET) {
				ExpNode* r = nodes.back();	
				nodes.pop_back();
				NextToken();
				CallNode* new_node = new CallNode(r, ParsExpression(parent, CLOSE_BRACKET));
				CheckTypeInCallNode(new_node, parent);
				nodes.push_back(new_node);				
			} else if (*GetToken() == OPEN_SQ_BRACKET) {
				ExpNode* r = nodes.back();	
				nodes.pop_back();
				NextToken();
				ArrNode* new_node = new ArrNode(r, ParsExpression(parent, CLOSE_SQ_BRACKET));
				CheckTypeInArrNode(new_node, parent);
				nodes.push_back(new_node);				
			} else if (*GetToken() == QUESTION) {
				ExpNode* c = nodes.back();	
				nodes.pop_back();
				NextToken();
				ExpNode* if_true = ParsExpression(parent, COLON);
				NextToken();
				ExpNode* if_false =  ParsExpression(parent, ExitCondition);
				TerOpNode* new_node = new TerOpNode(c, if_true, if_false);
				CheckTypeInTerOpNode(new_node, parent);
				nodes.push_back(new_node);				
				cur_position--;
			} else {
				operations.push_back(GetToken());
				prece_oper.push_back(GetPrecedencBinOper(GetToken()));
			}
			parent = parent_;
			NextToken();
		} else 
			throw_if(cur_position < tokens.size() && *GetToken() != ExitCondition, "Expect operation");
	}
	if (cur_position >= tokens.size() || *GetToken() == ExitCondition) {
		for (int i = operations.size(); i > 0; i--){
			ExpNode* r = nodes.back();
			nodes.pop_back();
			if (prece_oper.back() == PRIORITY_UNARY_OP)
			{
				UnOpNode* new_node = new UnOpNode(operations.back(), r);
				CheckTypeInUnOpNode(new_node);
				nodes.push_back(new_node);	
			} else {
				throw_if(!nodes.size(), "Expect identifier or number");
				ExpNode* l = nodes.back();
				nodes.pop_back();
				BinOpNode* new_node = new BinOpNode(operations.back(), r, l);
				CheckTypeInBinOpNode(new_node, parent);
				nodes.push_back(new_node);
			}
			operations.pop_back();
			prece_oper.pop_back();
		}
	} 
	throw_if(tokens.size() && cur_position == tokens.size() 
			  && *tokens[cur_position - 1] != ExitCondition && *ExitCondition != SEMICOLON,
				"Expect '" + ExitCondition->GetText() + '\'');
	return nodes.size() ? nodes[0] : new ValNode(new Identifier(0, 0, "", ""), NULL);
}
	

Token* Pars::GetToken()
{ 
	if(cur_position >= tokens.size())
		throw std::string("Unexpected end of file");
	return tokens[cur_position];
}


Token* Pars::NextToken()
{ 
	if(cur_position >= tokens.size())
		throw std::string("Unexpected end of file");
	return tokens[cur_position++];
}


bool SymbolScalar::operator == (SymbolType* e) const
{
	SymbolScalar* ss = dynamic_cast<SymbolScalar*>(e);
	return ss && this->type == ss->type;
}


bool SymbolArray::operator == (SymbolType* e) const
{
	SymbolArray* ss = dynamic_cast<SymbolArray*>(e);
	return ss && *this->basis_type == ss->basis_type;
}


bool SymbolPoint::operator == (SymbolType* e) const
{
	SymbolPoint* ss = dynamic_cast<SymbolPoint*>(e);
	return ss && *this->basis_type == ss->basis_type;
}