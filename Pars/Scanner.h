#ifndef SCANNER_H
#define SCANNER_H
#include <stdlib.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>


enum Token_type {
		identifier,
		my_char,
		string,
		number,
		my_float,
		operation,
		separator,
		key_words,
		type_name,
		error,
};


enum InOut
{
	Printf,
	Scanf
};


class Token {
private:
	int line;
	int column;
	std::string text;
	std::string value;
	Token_type type;

public:
	Token(int l, int c, std::string t, std::string v, Token_type tt) : line(l), column(c), text(t), value(v), type(tt){}
	int Line() { return line; }
	int Column() { return column; }	
	std::string GetText() const {return text;}
	std::string GetValue() const {return value;} 
	Token_type GetType() {return type;}
	std::string GetPosition() {return " line " + std::to_string(Line()) + ", column " + std::to_string(Column());};
	virtual std::string ClassName() { return "Token";};
	bool operator == (const Token*) const;
	bool operator != (const Token*) const;
};

 
class Number : public Token {
public:
	Number(int l, int c, std::string t, std::string v) : Token(l, c, t, v, number) {};
	std::string ClassName() { return "Number    "; }
};


class NumberFloat : public Token {
public:
	NumberFloat(int l, int c, std::string t, std::string v) : Token(l, c, t, v, my_float) {};
	std::string ClassName() { return "Float    "; }
};


class String: public Token {
public:
	String(int l, int c, std::string t, std::string v) : Token(l, c, t, v, string) {}
	std::string ClassName() { return "String    "; }
};


class Char: public Token {
public:
	Char(int l, int c, std::string t, std::string v) : Token(l, c, t, v, my_char) {}
	std::string ClassName() { return "Char    "; }
};



class Identifier: public Token {
public:
	Identifier(int l, int c, std::string t, std::string v, Token_type tt = identifier) : Token(l, c, t, v, tt) {}
	std::string ClassName() { return "Identifier"; }
};


class KeyWords: public Identifier {
public:
	KeyWords(int l, int c, std::string t, std::string v, Token_type tt = key_words) : Identifier(l, c, t, v, tt) {}
	std::string ClassName() { return "KeyWords  "; }
};


class Type: public KeyWords {
public:
	Type(int l, int c, std::string t, std::string v) : KeyWords(l, c, t, v, type_name) {}
	std::string ClassName() { return "Type Name  "; }
};


class Operation: public Token {
public:
	Operation(int l, int c, std::string t, std::string v) : Token(l, c, t, v, operation) {}
	std::string ClassName() { return "Operation "; }
};


class Separator: public Token {
public:
	Separator(int l, int c, std::string t, std::string v) : Token(l, c, t, v, separator) {}
	std::string ClassName() { return "Separator "; }
};


class Error: public Token {
public:
	Error(int l, int c, std::string t, std::string v) : Token(l, c, t, v, error) {}
	std::string ClassName() { return "Error    "; }
};

class Scanner {
private:
	FILE* file;
	int cur_line;
	int cur_column;
	Token* last_tokens;
	
	enum States {
		identifier,
		string,
		char_,
		number_int,
		number_float,
		number_hex,
		number_exp,
		comment,
		complex�omment,
		operation,
		complexOperation,
		separator,
		whitespace,
		block,
		error,
	};	
	
	States cur_state;
	std::string buffer;

public: 
	Scanner(FILE* inFile) : file(inFile), cur_column(1), cur_line(1), cur_state(whitespace), buffer(""), last_tokens(0) {}
	bool Next();
	Token* Get() {return last_tokens;};
};

#endif /* SCANNER_H */