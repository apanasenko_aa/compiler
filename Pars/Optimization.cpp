#include "Pars.h"
#include <algorithm>


ExpNode* UpdateChild(ExpNode* oldNode, ExpNode* newNode)
{
	if (newNode){
		if (newNode != oldNode)
			delete oldNode;
		return newNode;
	}
	return oldNode;
}


ValNode* CreateNode(SymbolType* type, int iValue, double fValue)
{
	std::string value;
	if (*type == INT) {
		value = std::to_string(iValue);
		fValue = iValue;
	}
	else {
		value = std::to_string(fValue);
	}
	ValNode* newNode = new ValNode(
		new Token(0, 0, value, value, number),
		type,
		iValue,
		fValue
		);
	return newNode;
}


Symbol* GetSymbol(Block*block, std::string name)
{
	if (!block) return nullptr;
	return block->tags.count(name) && block->tags[name] < block->symbols.size()
		? block->symbols[block->tags[name]]
		: GetSymbol(block->parent, name);
};


float ValNode::GetValue()
{
	return f_value;
}


int ValNode::GetInt()
{
	return i_value;
}


Statement* Node_if::Optimize(Block* block)
{
	ExpNode* newCondition = condition->Optimize(block);
	condition = UpdateChild(condition, newCondition);
	block_if = block_if->Optimize(block);
	bool returnAfterTrue = block->haveReturn || block_if->HaveReturn();
	bool returnAfterFalse = false;
	block->haveReturn = false;
	if (block_else){
		block_else = block_else->Optimize(block);
		returnAfterFalse = block->haveReturn || block_else->HaveReturn();
	}
	block->haveReturn = newCondition
		? (condition->GetValue() ? returnAfterTrue : returnAfterFalse)
		: returnAfterTrue && returnAfterFalse;
	return !newCondition ? this : (condition->GetValue() ? block_if : block_else);
}


Statement* Node_for::Optimize(Block* block)
{
	if (start)
		start = UpdateChild(start, start->Optimize(block));
	if (end){
		end = UpdateChild(end, end->Optimize(block));
		if (!end->GetValue()) 
			return NULL;
	}
	if (must_do)
		must_do = UpdateChild(must_do, must_do->Optimize(block));
	block_loop = block_loop->Optimize(block);
	return this;
}


Statement* Node_while::Optimize(Block* block)
{
	if (end->Optimize(block) && !end->GetValue())
		return NULL;
	block_loop->Optimize(block);
	return this;
}


Statement* Node_do::Optimize(Block* block)
{
	block_loop->Optimize(block);
	block->haveReturn = block->haveReturn || block_loop->HaveReturn();
	if (block->haveReturn || end->Optimize(block) && !end->GetValue())
		return block_loop;
	return this;
}


Statement* Node_return::Optimize(Block* block)
{
	if (value){
		value = UpdateChild(value, value->Optimize(block));
	}
	block->haveReturn = true;
	return this;
}


Statement* Node_block::Optimize(Block* block)
{
	this->block->Optimize();
	return this;
}


Statement* Node_expression::Optimize(Block* block)
{
	exp = UpdateChild(exp, exp->Optimize(block));
	return this;
}


ExpNode* UnOpNode::Optimize(Block* block)
{
	ExpNode* optimize = value->Optimize(block);
	int intValue;
	double f_value;
	if (optimize){
		value = UpdateChild(value, optimize);
		if (*type == INT){
			if (*operation == INVERSE){
				intValue = ~value->GetInt();
			}
			else if (*operation == PLUS){
				intValue = value->GetInt();
			}
			else if (*operation == MINUS){
				intValue = -value->GetInt();
			}
			else if (*operation == NEGATIVE){
				intValue = !value->GetInt();
			}
			else {
				return NULL;
			}
			f_value = intValue;
		}
		else
		{
			if (*operation == PLUS){
				f_value = value->GetValue();
			}
			else if (*operation == MINUS){
				f_value = -value->GetValue();
			}
			else if (*operation == NEGATIVE){
				f_value = !value->GetValue();
			}
			else {
				return NULL;
			}
		}
	}
	return optimize ? CreateNode(type, intValue, f_value) : NULL;
}


ExpNode* BinOpNode::Optimize(Block* block)
{
	ExpNode* r_optimize = right->Optimize(block);
	ExpNode* l_optimize = left->Optimize(block);
	right = UpdateChild(right, r_optimize);
	left = UpdateChild(left, l_optimize);
	int iValue = 0;
	double f_value;
	if (r_optimize && l_optimize){
		if (*type == INT) {
			int r = right->GetInt();
			int l = left->GetInt();
			if (*operation == PLUS){
				iValue = r + l;
			}
			else if (*operation == MINUS){
				iValue = l - r;
			}
			else if (*operation == DIV){
				iValue = l / r;
			}
			else if (*operation == MOD){
				iValue = l % r;
			}
			else if (*operation == MUL){
				iValue = l * r;
			}
			else if (*operation == BIT_OR){
				iValue = l | r;
			}
			else if (*operation == BIT_AND){
				iValue = l & r;
			}
			else if (*operation == BIT_XOR){
				iValue = l ^ r;
			}
			else if (*operation == BIT_MOVE_L){
				iValue = l << r;
			}
			else if (*operation == BIT_MOVE_R){
				iValue = l >> r;
			}
			else if (*operation == OR){
				iValue = l || r;
			}
			else if (*operation == AND){
				iValue = l && r;
			}
			else if (*operation == EQUAL){
				iValue = l == r;
			}
			else if (*operation == NOT_EQUAL){
				iValue = l != r;
			}
			else if (*operation == LESS){
				iValue = l < r;
			}
			else if (*operation == MORE){
				iValue = l > r;
			}
			else if (*operation == LESS_E){
				iValue = l <= r;
			}
			else if (*operation == MORE_E){
				iValue = l >= r;
			}
			else {
				return NULL;
			}
			f_value = iValue;
		}
		else {
			double l = left->GetValue();
			double r = right->GetValue();
			if (*operation == PLUS){
				f_value = r + l;
			}
			else if (*operation == MINUS){
				f_value = l - r;
			}
			else if (*operation == DIV){
				f_value = l / r;
			}
			else if (*operation == MUL){
				f_value = l * r;
			}
			else if (*operation == OR){
				f_value = l || r;
			}
			else if (*operation == AND){
				f_value = l && r;
			}
			else if (*operation == EQUAL){
				f_value = l == r;
			}
			else if (*operation == NOT_EQUAL){
				f_value = l != r;
			}
			else if (*operation == LESS){
				f_value = l < r;
			}
			else if (*operation == MORE){
				f_value = l > r;
			}
			else if (*operation == LESS_E){
				f_value = l <= r;
			}
			else if (*operation == MORE_E){
				f_value = l >= r;
			}
			else {
				return NULL;
			}
		}
	}
	return r_optimize && l_optimize ? CreateNode(type, iValue, f_value) : NULL;
}


ExpNode* TerOpNode::Optimize(Block* block)
{
	condition = UpdateChild(condition, condition->Optimize(block));
	if_true = UpdateChild(if_true, if_true->Optimize(block));
	if_false = UpdateChild(if_false, if_false->Optimize(block));
	return NULL;
}


ExpNode* CallNode::Optimize(Block* block)
{
	arg = UpdateChild(arg, arg->Optimize(block));
	return NULL;
}


ExpNode* ArrNode::Optimize(Block* block)
{
	right = UpdateChild(right, right->Optimize(block));
	left = UpdateChild(left, left->Optimize(block));
	return NULL;
}


ExpNode* ValNode::Optimize(Block* block)
{
	SymbolScalar* ss = dynamic_cast<SymbolScalar*>(type);
	bool result = ss && ss->isConst && (*ss == INT || *ss == FLOAT);
	if (result){
		if (value->GetType() == identifier){
			SymbolValue* sv = dynamic_cast<SymbolValue*>(GetSymbol(block, value->GetValue()));
			ExpNode* newValue = sv->value->Optimize(block);
			if (sv && newValue){
				if (*ss == INT)
					i_value = newValue->GetInt();
				f_value = newValue->GetValue();
				sv->value = UpdateChild(sv->value, newValue);
			}
		}
		else {
			if (*ss == INT)
				i_value = atoi(value->GetValue().c_str());
			f_value = atof(value->GetValue().c_str());
		}
	}
	return result ? CreateNode(type, i_value, f_value) : NULL;
}


void SymbolFunc::Optimize(Block* block){
	body->Optimize();
}


void SymbolValue::Optimize(Block* block){
	if (value){
		value = UpdateChild(value, value->Optimize(block));
	}
}


Block* Block::Optimize()
{
	int statmentSize = statements.size();
	for (int i = 0; i < symbols.size(); i++){
		symbols[i]->Optimize(this);
	}
	for (int i = 0; i < statements.size(); i++){
		if (haveReturn){
			statmentSize = std::min(statmentSize, i);
			delete statements[i];
		} else {
			Statement* s = statements[i]->Optimize(this);
			if (s != statements[i]){
				delete statements[i];
				statements[i] = s;
			}
		}
	}
	statements.resize(statmentSize);	
	return this;
}
 