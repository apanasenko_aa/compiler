#ifndef ASM_COMMANDS_H
#define ASM_COMMANDS_H

#include <string>
#include <vector>
#include <map>
#include "Scanner.h"


enum asm_command 
{
	cmdDD,
	cmdDB,
	cmdDQ,
	cmdMOV,
	cmdPUSH,
	cmdPOP,
	cmdMUL,
	cmdIMUL,
	cmdDIV,
	cmdIDIV,
	cmdADD,
	cmdSUB,
	cmdINC,
	cmdDEC,
	cmdRET,
	cmdNEG,
	cmdCDQ,
	cmdINVOKE,
	cmdSHR,
	cmdSHL,
	cmdAND,
	cmdOR,
	cmdXOR,
	cmdNOT,
	cmdCALL,
	cmdJMP,
	cmdCMP,
	cmdJE,
	cmdJNE,
	cmdSETG,
	cmdSETL,
	cmdSETGE,
	cmdSETLE,
	cmdSETE,
	cmdSETNE,
};


enum asm_register
{
	EAX,
	EBX,
	ECX,
	EDX,
	EBP,
	ESP,
	CL,
	AL,
};


class AsmArgument
{
protected:
	static std::map<asm_register, std::string> RegToStr; 
public:
	virtual std::string generate() {return "";};
	AsmArgument();
};


class ArgImmediate : public AsmArgument
{
private:
	int value;
public:
	std::string generate() {return std::to_string(value);};
	ArgImmediate(int v)
		: value(v) 
	{};
};


class ArgString : public AsmArgument
{
private:
	std::string value;
public:
	std::string generate() {return value;};
	ArgString(std::string& str)
		: value(str)
	{};
};


class ArgRegister : public AsmArgument
{
protected:
	asm_register reg;
public:
	std::string generate() {return RegToStr[reg];};
	ArgRegister(asm_register r)
		: reg(r) 
	{};
};


class ArgIndirect : public ArgRegister
{
private:
	int offset;
public:
	std::string generate() {return "dword ptr [" + RegToStr[reg] + " + " + std::to_string(offset) + "]";};
	ArgIndirect(asm_register r, int shift = 0) 
		: ArgRegister(r)
		, offset(shift) 
	{};
};


class ArgMemory : public AsmArgument
{
private:
	std::string varName;
public:
	std::string generate() {return varName;};
	ArgMemory(std::string& name)
		: varName(name) 
	{};
};


class ArgLabel : public AsmArgument
{
private:
	std::string name;
public:
	std::string generate() {return name;};
	ArgLabel(std::string& n) 
		: name(n) 
	{};
};


class ArgDup : public AsmArgument
{
private:
	int count;
public:
	std::string generate() {return std::to_string(count) + " dup(0)";};
	ArgDup(int c)
		: count(c)
	{};
};


//__________________Instruction___________________//


class AsmInstruction 
{
protected:
	static std::map<asm_command, std::string> CommandToStr;	
public:
	virtual std::string generate() {return "";};
	AsmInstruction();
};


class InstrLabel : public AsmInstruction
{
protected:
	ArgLabel* label;
public:
	std::string generate();
	InstrLabel(ArgLabel* l)
		: label(l) 
	{};
};


class AsmCmd : public AsmInstruction
{
protected:
	asm_command opCode;
public:
	std::string generate();
	AsmCmd() {};
	AsmCmd(asm_command code)
		: opCode(code) 
	{};
};


class AsmCmd_1 : public AsmCmd
{
private:
	AsmArgument* arg;
public:
	std::string generate();
	AsmCmd_1(asm_command code, AsmArgument* a)
		: AsmCmd(code)
		, arg(a) 
	{};
};


class AsmCmd_2 : public AsmCmd
{
private:
	AsmArgument* arg1;
	AsmArgument* arg2;
public:
	std::string generate();
	AsmCmd_2(asm_command code, AsmArgument* a1, AsmArgument* a2)
		: AsmCmd(code)
		, arg1(a1)
		, arg2(a2) 
	{};
};


class AsmInOut : public AsmCmd
{
private:
	InOut io;
	ArgMemory* format;
	AsmArgument* arg;
public:
	std::string generate();
	AsmInOut(InOut _io, ArgMemory* f, AsmArgument* a)
		: AsmCmd(cmdINVOKE)
		, io(_io)
		, format(f)
		, arg(a) 
	{}
};


class AsmCode
{
private:
	std::vector<AsmInstruction*> commands;
	int id;
public:
	void Print(FILE* f);
	AsmCode& add(AsmCmd* cmd);
	AsmCode& add(ArgLabel* label);
	AsmCode& add(asm_command cmd);
	AsmCode& add(asm_command cmd, AsmArgument* arg);
	AsmCode& add(asm_command cmd, asm_register reg);
	AsmCode& add(asm_command cmd, asm_register reg, int val);
	AsmCode& add(asm_command cmd, AsmArgument* arg1, AsmArgument* arg2);
	AsmCode& add(asm_command cmd, asm_register reg1, asm_register reg2);
	AsmCode& add(InOut io, ArgMemory* format, AsmArgument* a = 0);
	int GetID() 
	{
		return id++;
	};
	AsmCode()
		: commands(0) 
		, id(0)
	{};
};

#endif //ASM_COMMANDS_H