#ifndef PARS_H
#define PARS_H

//#include "Scanner.h"
#include "CodeGenerator.h"
#include <map>
#include <string>

const int HIGHER_PRIORITY = 16;
const int PRIORITY_UNARY_OP = HIGHER_PRIORITY - 1;

extern const Token* INVERSE;
extern const Token* PLUS;
extern const Token* MINUS;
extern const Token* AMPERSAND;
extern const Token* NEGATIVE;

extern const Token* BIT_OR;
extern const Token* BIT_AND;
extern const Token* BIT_XOR;
extern const Token* BIT_MOVE_L;
extern const Token* BIT_MOVE_R;
extern const Token* OR;
extern const Token* AND;
extern const Token* EQUAL;
extern const Token* NOT_EQUAL;
extern const Token* LESS;
extern const Token* MORE;
extern const Token* LESS_E;
extern const Token* MORE_E;
extern const Token* MUL;
extern const Token* DIV;
extern const Token* MOD;

extern const Token* A_PLUS;
extern const Token* A_MINUS;
extern const Token* A_MUL;
extern const Token* A_DIV;
extern const Token* A_MOD;
extern const Token* A_XOR;
extern const Token* A_OR;
extern const Token* A_AND;

extern const Token* COMMA;
extern const Token* POINT;
extern const Token* ARROW;
extern const Token* INCREMENT;
extern const Token* DECREMENT;
extern const Token* ASSIGN;
extern const Token* POINTER;


enum symbol_type {
		_name_,
		_struct_,
		_func_,
		_typedef_,
		_class_,
};

class Block;

class Statement {
public:
	virtual Statement* Optimize(Block* block){ return NULL; };
	virtual void Print(std::string d, FILE* f) {};
	virtual void Generate(AsmCode& data, AsmCode& code){};
	virtual bool HaveReturn() { return false; }
};


class Symbol {
public:
	std::string name;
	symbol_type s_type;
	virtual void Optimize(Block* block){};
	virtual void Print(std::string d, FILE* f) {};
	virtual void Generate(AsmCode& data, AsmCode& code){};
	Symbol(std::string name_, symbol_type st_)
		:name(name_)
		,s_type(st_)
	{};	
	Symbol(){};
};


class Block {
private:
	
public:
	Block* parent;
	bool haveParams;
	bool haveReturn = false;
	std::vector<Symbol*> symbols;
	std::map<std::string, int> tags;
	std::vector<Statement*> statements;
	void add(Symbol* s) // move in Pars.cpp
	{
		tags[s->name] = symbols.size();
		symbols.push_back(s);
	}
	void add(Statement* s) {statements.push_back(s);}
	void Print(std::string d, FILE* f);
	Block* Optimize();
	void GlobalGenerate(CodeGenerator* generator);
	Block(Block* parent_, bool hp = false)
		: parent(parent_)
		, haveParams(hp)
	{}
};


class SymbolType : public Symbol {
public:
	void Print(std::string d, FILE* f){};
	virtual bool operator == (SymbolType*) const { return false; };
	virtual SymbolType* GetBasisType() {return this;};
	virtual std::string toString() {return "";};
	virtual bool CanConvertTo(SymbolType* st){return false;};
	SymbolType(){};
};


class ExpNode {
public:
	SymbolType* type;
	virtual ExpNode* Optimize(Block* block){ return NULL; };
	virtual void Print(std::string d, FILE* f) {};
	virtual void GenerateValue(AsmCode&	code){};
	virtual void GenerateAddres(AsmCode& code){};
	virtual float GetValue() { return 0; }
	virtual int GetInt() { return 0; }
	ExpNode()
	{}
	ExpNode(SymbolType* st) 
		: type(st) 
	{}
};


class SymbolScalar : public SymbolType {
public:
	bool isConst;
	std::string type;
	std::string toString() {return type;};
	void Print(std::string d, FILE* f);
	bool operator == (SymbolType*) const;
	bool CanConvertTo(SymbolType* st);
	//bool operator != (SymbolType*) const;
	SymbolType* GetBasisType() {return this;};
	SymbolScalar(std::string t, bool isConst_)
		: type(t)
		, isConst(isConst_)
	{};
};


class SymbolArray : public SymbolType {
public:
	int size;
	SymbolType* basis_type;
	std::string toString() {return "[" + basis_type->toString() + " x " + std::to_string(size) + "]";};
	void Print(std::string d, FILE* f);
	bool operator == (SymbolType*) const;
	bool CanConvertTo(SymbolType* st);
	SymbolType* GetBasisType() {return basis_type;};
	SymbolArray(SymbolType* basis, int size_)
		: basis_type(basis)
		, size(size_)
	{}
};


class SymbolPoint : public SymbolType {
public:
	SymbolType* basis_type;
	std::string toString() {return "*(" + basis_type->toString() + ")";};
	void Print(std::string d, FILE* f);
	bool operator == (SymbolType*) const;
	bool CanConvertTo(SymbolType* st);
	SymbolType* GetBasisType() {return basis_type;};
	SymbolPoint(SymbolType* basis)
		: basis_type(basis)
	{}
};


class SymbolWithType : public Symbol {
public:
	SymbolType* type;
	void Print(std::string d, FILE* f){};
	SymbolWithType (SymbolType* type_,  std::string name_, symbol_type st_) 
		: type(type_)
		, Symbol(name_, st_)
	{};
};


class SymbolValue : public SymbolWithType {
public:
	ExpNode* value;
	void Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void Generate(AsmCode& data, AsmCode& code);
	SymbolValue(SymbolType* type,  std::string name, ExpNode* value_ = NULL) 
		: value(value_) 
		, SymbolWithType(type, name, _name_)
	{};
};


class SymbolFunc : public SymbolWithType {
public:
	Block* params;
	Block* body;
	void Optimize(Block* block);
	void Print(std::string d, FILE* f);
	SymbolFunc(SymbolType* type,  std::string name, Block* params_, Block* body_ = NULL) 
		: params(params_) 
		, body(body_)
		, SymbolWithType(type, name, _func_)
	{};	
};


class SymbolStruct : public Symbol {
public:
	Block* body;
	void Print(std::string d, FILE* f);
	SymbolStruct(std::string name_, Block* body_) 
		: body(body_)
		, Symbol(name_, _struct_)
	{};	
};


class SymbolTypedef : public Symbol {
public:
	SymbolType* type;
	void Print(std::string d, FILE* f);
	SymbolTypedef(std::string n, SymbolType* t)
		: Symbol(n, _typedef_)
		, type(t)
	{};
};


class SymbolClass : public Symbol {
public:
	//std::vector<SymbolFunc*> metheds;
	//std::vector<SymbolValue*> propertes;
	Block* body;
	SymbolClass* parent;
	void Print(std::string d, FILE* f);
	SymbolClass(std::string n, SymbolClass* p, Block* b)
		: Symbol(n, _class_)
		, parent(p)
		, body(b)
	{};
};


class UnOpNode : public ExpNode {
public:
	Token* operation;
	ExpNode* value;
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	void GenerateValue(AsmCode&	code);
	void GenerateAddres(AsmCode& code);
	UnOpNode(Token* o, ExpNode* v) 
		: operation(o)
		, value(v) 
	{};
};


class BinOpNode : public ExpNode {
public:
	Token* operation;
	ExpNode* left;
	ExpNode* right;
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void GenerateValue(AsmCode&	code);
	void GenerateAddres(AsmCode& code);
	BinOpNode (Token* v, ExpNode* r, ExpNode* l) 
		: operation(v)
		, right(r)
		, left(l)
	{};
};


class TerOpNode : public ExpNode {
public:
	ExpNode* condition;
	ExpNode* if_true;
	ExpNode* if_false;
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void GenerateValue(AsmCode&	code){};///////////////////////////////////////////////////////////// no definition
	TerOpNode(ExpNode* c, ExpNode* t, ExpNode* f) 
		: condition(c)
		, if_true(t)
		, if_false(f)
	{};
};


class ValNode : public ExpNode {
private:
	double f_value;
	int i_value;
public:
	Token* value;
	float GetValue();
	int GetInt();
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void GenerateValue(AsmCode&	code);
	void GenerateAddres(AsmCode& code);
	ValNode(Token* t, SymbolType* st)
		: value(t) 
		, ExpNode(st)
	{}
	ValNode(Token* t, SymbolType* st, int i, double f)
		: value(t)
		, ExpNode(st)
		, i_value(i)
		, f_value(f)
	{}
};


class CallNode : public ExpNode {
public:
	ExpNode* name;
	ExpNode* arg;
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void GenerateValue(AsmCode&	code){};///////////////////////////////////////////////////////////// no definition
	CallNode(ExpNode* l, ExpNode* r)
		: name(l)
		, arg(r) 
	{};
};


class ArrNode : public ExpNode {
public:
	ExpNode* left;
	ExpNode* right;
	ExpNode* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	void GenerateValue(AsmCode&	code){};///////////////////////////////////////////////////////////// no definition
	void GenerateAddres(AsmCode& code){};///////////////////////////////////////////////////////////// no definition
	ArrNode(ExpNode* l, ExpNode* r)
		: left(l)
		, right(r)
	{};
};


class Node_if : public Statement {
private:
	ExpNode* condition;
	Statement* block_if;
	Statement* block_else;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f);
	Node_if(ExpNode* con, Statement* b_if, Statement* b_else)
		: condition  (con)
		, block_if   (b_if)
		, block_else (b_else)
	{};
};


class Node_for : public Statement {
private:
	ExpNode* start;
	ExpNode* end;
	ExpNode* must_do;
	Statement* block_loop;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	Node_for(ExpNode* s, ExpNode* e, ExpNode* md, Statement* bl)
		: start      (s)
		, end        (e)
		, must_do    (md)
		, block_loop (bl)
	{};
};


class Node_while : public Statement {
private:
	ExpNode* end;
	Statement* block_loop;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	Node_while(ExpNode* e, Statement* bl)
		: end        (e)
		, block_loop (bl)
	{};
};


class Node_do : public Statement {
private:
	ExpNode* end;
	Statement* block_loop;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	Node_do(ExpNode* e, Statement* bl)
		: end        (e)
		, block_loop (bl)
	{};
};


class Node_break : public Statement {
public:
	void Print(std::string d, FILE* f); 
};


class Node_continue : public Statement {
public:
	void Print(std::string d, FILE* f); 
};


class Node_printf : public Statement {
private:
	std::string format;
	std::vector<ExpNode*> node;

public:
	void Generate(AsmCode& data, AsmCode& code);
	void Print(std::string d, FILE* f); 
	void add(ExpNode* n)
	{
		node.push_back(n);
	}
	Node_printf(std::string _format)
		: format(_format)
		, node(0)
	{};
};


class Node_return : public Statement {
private:
	ExpNode* value;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	Node_return(ExpNode* v)
		: value(v)
	{};
};


class Node_block : public Statement {
private:
	Block* block;
public:
	Statement* Optimize(Block* block);
	bool HaveReturn(){ return block->haveReturn; }
	void Print(std::string d, FILE* f); 
	Node_block(Block* b)
		: block(b)
	{};
};


class Node_expression : public Statement {
private:
	ExpNode* exp;
public:
	Statement* Optimize(Block* block);
	void Print(std::string d, FILE* f); 
	Node_expression(ExpNode* e)
		: exp(e)
	{};
};


class Pars {
private:
	bool genCode;
	int cur_position;	
	std::vector <Token*> tokens;	
	SymbolType* curBlockType;
	
	Block* ParsFuncParams (Block* parent);
	Block* ParsBlock (Block* parent, const Token* ExitCondition, bool HaveParams = false);
	Symbol* ParsStruct (Block* parent, bool isConst, bool isFuncPapams = false);
	ExpNode* ParsExpression (Block* parent, const Token* ExitCondition);
	ExpNode* ParsCondition (Block* parent);
	Statement* ParsStatement (Block* parent);	
	SymbolWithType* ParsArray (SymbolWithType* basis_type, const Token* ExitCondition);
	SymbolWithType* ParsSymbol (Block* parent, const Token* ExitCondition, SymbolType* basis_type,
								bool isFuncPapams = false,  bool mustReturn = false);
	SymbolTypedef* ParsTypedef (Block* parent);	
	SymbolClass* ParsClass(Block* parent);
	SymbolType* GetTypeIdentByName(Block* block,  std::string name);
	SymbolType* GetTypeFuncByName(Block* block,  std::string name);
	bool isType (Token* token, Block* block);
	bool IsNameDeclaration (std::string name, Block* block, bool search_in_parent, 
		symbol_type WhatWeAreLookingFor = _name_,  bool HaveFuncBody = false, bool inExp = false);
	void ParsDeclaration (Block* block, Block* parent);
	void throw_if (bool condition, std::string message);
	void CheckTypeInUnOpNode(UnOpNode* node);
	void CheckTypeInBinOpNode(BinOpNode* node, Block* block);
	void CheckTypeInTerOpNode(TerOpNode* node, Block* block);
	void CheckTypeInCallNode(CallNode* node, Block* block);
	void CheckTypeInArrNode(ArrNode* node, Block* block);
	void CheckConst(SymbolType* st, std::string op);
	void CheckInt(SymbolType* st, std::string op, std::string pos);
	void CheckFloat(SymbolType* st, std::string op, std::string pos);
	void CheckVoid(SymbolType* st, std::string op, std::string pos);
	bool isConstNode(SymbolType* type);

	std::vector<SymbolType*> GetListOfTypesOfParams(std::string name_func, Block* block);
	std::map<std::string, Symbol*> GetMapOfStruct(std::string name_struct, Block* block);
	Token* GetToken();
	Token* NextToken();

	CodeGenerator* generator;
public:
	ExpNode* GetExpression();
	Block* GetSymbolTable();
	void generateCode();

	Pars(std::vector <Token*> inTokins) 
		: tokens(inTokins)
		, cur_position(0) 
		, genCode(false)
	{};
	Pars(std::vector <Token*> inTokins, CodeGenerator* cg) 
		: tokens(inTokins)
		, generator(cg)
		, cur_position(0)
		, genCode(true)
	{};
};


extern const ValNode* EMPTY;
extern SymbolScalar* INT;
extern SymbolScalar* FLOAT;
extern SymbolScalar* CHAR;
extern SymbolScalar* VOID;
extern SymbolPoint* STRING;

#endif //PARS_H