#ifndef CODE_GENERATOR_H
#define CODE_GENERATOR_H

#include <string>
#include <vector>
#include "ASM_commands.h"


ArgImmediate* Immediate(int a);
ArgMemory* Memory(std::string& varName);
ArgRegister* Register(asm_register reg);
ArgIndirect* Indirect(asm_register reg, int offset = 0);
ArgString* Stringf(std::string& format);
ArgDup* Dup(int count);

extern std::string MASM_Addres;

class CodeGenerator
{
private:
	FILE* file_name;
	AsmCode data;
	AsmCode code;
public:
	friend class Pars;
	void generate();
	CodeGenerator(FILE* file)
		: file_name(file) 
	{};	
};

#endif