#include "Pars.h"


void Block::GlobalGenerate(CodeGenerator* generator)
{
	for (int i = 0; i < symbols.size(); i++){
		switch (symbols[i]->s_type)
		{
		case _name_ :

			break;
		case _func_:

			break;
		default:
			break;
		}
	}
}


static bool isAssignment(Token* op) 
{
	return *op == ASSIGN 
		|| *op == A_PLUS
		|| *op == A_MINUS 
		|| *op == A_MUL 
		|| *op == A_DIV
		|| *op == A_MOD
		|| *op == A_AND
		|| *op == A_XOR
		|| *op == A_OR;
}


void ValNode::GenerateValue(AsmCode& code)
{
	switch (value->GetType())
	{
	case identifier:
		code.add(cmdPUSH, Memory("dword ptr [g_v_" + value->GetValue() + "]"));
		break;
	case my_char:
		code.add(cmdPUSH,Immediate(value->GetValue()[0]));
		break;
	case number:
		code.add(cmdPUSH,Immediate(stoi(value->GetValue())));
		break;
	default:
		
		break;
	}
}


void ValNode::GenerateAddres(AsmCode& code)
{
	if (value->GetType() != identifier)
		throw std::string("Error lvalue, argument must be identifier");
	code.add(cmdPUSH, Memory("offset g_v_" + value->GetValue()));
}


void UnOpNode::GenerateValue(AsmCode& code)
{
	Token op = *operation;
	if (op == AMPERSAND)
	{
		value->GenerateAddres(code);
	}
	else if (op == INCREMENT || op == DECREMENT)
	{
		value->GenerateValue(code);
		code.add(cmdPOP, EAX);
		code.add(op == INCREMENT ? cmdINC : cmdDEC, EAX);
		code.add(cmdPUSH, EAX);
		value->GenerateAddres(code);
		code.add(cmdPOP, EBX)
			.add(cmdPOP, EAX)
			.add(cmdMOV, Indirect(EBX), Register(EAX))
			.add(cmdPUSH, EAX);
	} 
	else if (op == NEGATIVE) {
		value->GenerateValue(code);
		code.add(cmdPOP, EAX)
			.add(cmdCMP, EAX, 0)
			.add(cmdSETE, AL)
			.add(cmdPUSH, EAX);
	} else {
		value->GenerateValue(code);
		code.add(cmdPOP, EAX);
		if (op == MINUS)
		{	
			code.add(cmdNEG, EAX);
		}
		else if (op == INVERSE)
		{
			code.add(cmdNOT, EAX);
		}
		else if (op == MUL)
		{
			code.add(cmdMOV, EBX, EAX)
				.add(cmdMOV, Register(EAX), Indirect(EBX));
		}	
		code.add(cmdPUSH, EAX);
	}
}


void UnOpNode::GenerateAddres(AsmCode& code)
{
	throw std::string("UnOpNode::GenerateAddres ((");
}


void BinOpNode::GenerateValue(AsmCode& code)
{
	Token op = *operation;
	AsmArgument *l, *r;
	right->GenerateValue(code);
	if (op == ASSIGN)
	{
		left->GenerateAddres(code);
		code.add(cmdPOP, EAX)
			.add(cmdPOP, EBX)
			.add(cmdMOV, Indirect(EAX), Register(EBX))
			.add(cmdMOV, EAX, EBX);
	} else {
		if (isAssignment(&op))
		{
			left->GenerateAddres(code);
			l = Indirect(EAX);
			r = Register(EBX);				
			code.add(cmdPOP, EAX)
				.add(cmdPOP, EBX);
		} else {
			l = Register(EAX);
			r = Register(EBX);
			left->GenerateValue(code);	
			code.add(cmdPOP, EAX)
				.add(cmdPOP, EBX);
		} 
		if (op == COMMA) 
			code.add(cmdMOV, EAX, EBX);			
		else if (op == DIV || op == MOD) {
			code.add(cmdCDQ)
				.add(cmdIDIV, r);
			if (op == MOD)
				code.add(cmdMOV, l, Register(EDX));
		} else if (op == A_DIV || op == A_MOD) {
			code.add(cmdMOV, ECX, EAX)
				.add(cmdMOV, Register(EAX), Indirect(ECX))
				.add(cmdCDQ)
				.add(cmdIDIV, r)
				.add(cmdMOV, Indirect(ECX), Register(op == A_MOD ? EDX : EAX));
			if (op == A_MOD)
				code.add(cmdMOV, EAX, EDX);
		} 
		else if (op == A_MUL)
		{
			code.add(cmdIMUL, r, l)
				.add(cmdMOV, l, r)
				.add(cmdMOV, Register(EAX), r);
		} 
		else if (op == BIT_MOVE_L || op == BIT_MOVE_R) 
		{
			code.add(cmdMOV, Register(ECX), r)
				.add(op == BIT_MOVE_L ? cmdSHL : cmdSHR, l, Register(CL));
		}
		else if (op == LESS  || op == LESS_E
			  || op == MORE  || op == MORE_E
			  || op == EQUAL || op == NOT_EQUAL) 
		{
			asm_command cmd = op == LESS ? cmdSETL
			                : op == LESS_E ? cmdSETLE 
							: op == MORE ? cmdSETG
							: op == MORE_E ? cmdSETGE
							: op == EQUAL ? cmdSETE
			/* NOT_EQUAL */ : cmdSETNE;

			code.add(cmdCMP, l, r);
			code.add(cmd, AL);
		} 
		else if (op == AND || op == OR) 
			code.add(op == AND ? cmdIMUL : cmdADD, EAX, EBX)
				.add(cmdCMP, EAX, 0)
				.add(cmdSETNE, AL);
		else {
			asm_command cmd = op == PLUS  || op == A_PLUS  ? cmdADD
				            : op == MINUS || op == A_MINUS ? cmdSUB
							: op == BIT_AND || op == A_AND ? cmdAND
							: op == BIT_OR  || op == A_OR  ? cmdOR
							: op == BIT_XOR || op == A_XOR ? cmdXOR
			/* op == MUL */ : cmdIMUL;
			
			code.add(cmd, l, r);
			if (isAssignment(&op))
				code.add(cmdMOV, ECX, EAX)
					.add(cmdMOV, Register(EAX), Indirect(ECX));
		}
	}
	code.add(cmdPUSH, EAX);
}


void BinOpNode::GenerateAddres(AsmCode& code)
{
	Token op = *operation;
	if (isAssignment(&op)) {
		GenerateValue(code);
		code.add(cmdPOP, EAX);
		left->GenerateAddres(code);
	}
}


void Node_printf::Generate(AsmCode& data, AsmCode& code)
{
	for (int i = node.size() - 1; i >= 0; i--)
	{
		node[i]->GenerateValue(code);
	} 
	int id = data.GetID();
	data.add(cmdDB, Memory("str" + std::to_string(id)), Stringf('"' + format + '"'));
	code.add(Printf, Memory("str" + std::to_string(id)));
	code.add(cmdADD, ESP, node.size() * 4); ////////////////////////////////////////////////////   TODO
}


void SymbolValue::Generate(AsmCode& data, AsmCode& code)
{
	data.add(cmdDD, Memory("g_v_" + name), Dup(1));
	if (value)
	{
		value->GenerateValue(code);
		code.add(cmdPOP,EAX)
			.add(cmdMOV, Memory("dword ptr [g_v_" + name + "]"), Register(EAX));
	}
}