#include "Pars.h"


static bool CanConvertToInt(SymbolType* st)
{
	return *st == INT || *st == CHAR;
}


static bool CanConvertToFloat(SymbolType* st)
{
	return *st == INT || *st == CHAR || *st == FLOAT;
}


static bool isPointer(SymbolType* st)
{
	return dynamic_cast<SymbolPoint*>(st);
}


static bool isVoid(SymbolType* st)
{
	return *st == VOID;
}


static bool CanAssignBinOp(BinOpNode* node)
{
	Token op = *node->operation;
	int a = op == POINT;
	return op == POINT
		|| op == ARROW
		|| op == ASSIGN
		|| op == A_PLUS
		|| op == A_MINUS		
		|| op == A_MUL
		|| op == A_DIV
		|| op == A_MOD
		|| op == A_XOR
		|| op == A_OR
		|| op == A_AND
		|| op == POINT;
}



static bool CanAssign(ExpNode* node)
{
	ValNode* vNode = dynamic_cast<ValNode*>(node);
	BinOpNode* bNode = dynamic_cast<BinOpNode*>(node);
	UnOpNode* uNode = dynamic_cast<UnOpNode*>(node);
	return vNode && vNode->value->GetType() == identifier
		|| dynamic_cast<ArrNode*>(node)
		|| bNode && CanAssignBinOp(bNode)
		|| uNode && *uNode->operation == POINTER;
}


bool Pars::isConstNode(SymbolType* type)
{
	return dynamic_cast<SymbolScalar*>(type) 
		 ? dynamic_cast<SymbolScalar*>(type)->isConst 
		 : isConstNode(type->GetBasisType()); 
}


bool SymbolScalar::CanConvertTo(SymbolType* st)
{
	SymbolScalar* ss = dynamic_cast<SymbolScalar*>(st);
	return ss && *ss == this;
}


bool SymbolPoint::CanConvertTo(SymbolType* st)
{
	SymbolPoint* ss = dynamic_cast<SymbolPoint*>(st);
	return ss && *ss == this;
}


bool SymbolArray::CanConvertTo(SymbolType* st)
{
	SymbolArray* ss = dynamic_cast<SymbolArray*>(st);
	return ss && *ss == this;
}


void Pars::CheckConst(SymbolType* st, std::string op)
{
	throw_if(isConstNode(st), " left argument cannot be const in operation " + op);
}


void Pars::CheckInt(SymbolType* st, std::string pos, std::string op)
{
	throw_if(!CanConvertToInt(st), pos + " argument must be int in operation " + op);
}


void Pars::CheckFloat(SymbolType* st, std::string pos, std::string op)
{
	throw_if(!CanConvertToFloat(st), pos + " argument must be number in operation " + op);
}


void Pars::CheckVoid(SymbolType* st, std::string pos, std::string op)
{
	throw_if(isVoid(st), pos + " argument cannot be void in operation " + op);
}


std::vector<SymbolType*> Pars::GetListOfTypesOfParams(std::string name_func, Block* block)
{
	throw_if(!block, "Function '" + name_func + "' not declared");   
	if (block->tags.count(name_func) &&  block->symbols[block->tags[name_func]]->s_type == _func_)
	{
		std::vector<SymbolType*> result;
		Block* params = dynamic_cast<SymbolFunc*>(block->symbols[block->tags[name_func]])->params;
		for (int i = 0; i < params->symbols.size(); i++)
		{
			result.push_back(dynamic_cast<SymbolWithType*>(params->symbols[i])->type);
		}
		return result;
	}
	else 
		return GetListOfTypesOfParams(name_func, block->parent);
}


std::map<std::string, Symbol*> Pars::GetMapOfStruct(std::string name_struct, Block* block)
{
	Block* structBlock = NULL;
	SymbolClass* curClass = NULL;

	while (!structBlock)
	{
		if (block->tags.count(name_struct) && block->symbols[block->tags[name_struct]]->s_type == _struct_)
			structBlock = dynamic_cast<SymbolStruct*>(block->symbols[block->tags[name_struct]])->body;
		else if (block->tags.count(name_struct) && block->symbols[block->tags[name_struct]]->s_type == _class_){
			curClass = dynamic_cast<SymbolClass*>(block->symbols[block->tags[name_struct]]);
			structBlock = curClass->body;
		}
		else
			block = block->parent;
	}
	std::map<std::string, Symbol*> result;
	do {
		for (int i = 0; i < structBlock->symbols.size(); i++)
		{
			Symbol* s = structBlock->symbols[i];
			if (result.count(s->name)) continue;
			result[s->name] = s;
		}
		structBlock = NULL;
		if (curClass && curClass->parent) {
			curClass = curClass->parent;
			structBlock = curClass->body;
		}
	} while (structBlock);
	return result;
}


SymbolType* Pars::GetTypeFuncByName(Block* block, std::string name)
{
	return (block->tags.count(name) && block->symbols[block->tags[name]]->s_type == _func_) 
		 ? dynamic_cast<SymbolWithType*>(block->symbols[block->tags[name]])->type
		 : GetTypeFuncByName(block->parent, name);
}



void Pars::CheckTypeInUnOpNode(UnOpNode* node)
{
	Token op = *node->operation;
	if (op == INCREMENT || op == DECREMENT)
	{
		CheckInt(node->value->type, "", op.GetValue());
		throw_if(!CanAssign(node->value), "argument must be lvalue");
		CheckConst(node->value->type, op.GetValue());
		node->type = node->value->type;
	}
	else if (op == PLUS	|| op == MINUS || op == INVERSE)
	{
		CheckInt(node->value->type, "", op.GetValue());
		node->type = node->value->type;
	}
	else if (op == AMPERSAND)
	{
		node->type = new SymbolPoint(node->value->type);
	}
	else if (op == POINTER)
	{
		throw_if(!isPointer(node->value->type), "Argument must be pointer");
		node->type = node->value->type->GetBasisType();
	}
	else if (op == NEGATIVE)
	{
		CheckVoid(node->value->type, "", op.GetValue());
		node->type = node->value->type->GetBasisType();
	} 
	else
		throw_if(true, "Undetected unary operation");
}


void Pars::CheckTypeInTerOpNode(TerOpNode* node, Block* block)
{
	CheckVoid(node->condition->type, "condition", "");
	throw_if(!(node->if_false->type->CanConvertTo(node->if_true->type) || node->if_true->type->CanConvertTo(node->if_false->type)),
		"Error type: right argument cannot to convert in operation TerOp");
	node->type = node->if_true->type;
}


void Pars::CheckTypeInBinOpNode(BinOpNode* node, Block* block)
{
	Token op = *node->operation;
	if (op == BIT_AND || op == BIT_OR || op == BIT_XOR || op == MOD || op == BIT_MOVE_L || op == BIT_MOVE_R)
	{
		CheckInt(node->left->type, "left", op.GetValue());
		CheckInt(node->right->type, "right", op.GetValue());
		node->type = INT;
	}
	else if (op == AND	|| op == OR || op == EQUAL || op == NOT_EQUAL)
	{
		CheckVoid(node->left->type, "left", op.GetValue());
		CheckVoid(node->right->type, "right", op.GetValue());
		node->type = INT;
	}
	else if (op == LESS	|| op == LESS_E || op == MORE || op == MORE_E)
	{
		CheckFloat(node->left->type, "left", op.GetValue());
		CheckFloat(node->right->type, "right", op.GetValue());
		node->type = INT;
	}
	else if (op == MUL || op == DIV || op == PLUS || op == MINUS)
	{
		CheckFloat(node->left->type, "left", op.GetValue());
		CheckFloat(node->right->type, "right", op.GetValue());
		node->type = *node->left->type == INT && *node->right->type == INT ? INT : FLOAT;
	}
	else if (op == COMMA)
	{
		node->type = node->right->type;
	}
	else if (op == A_AND || op == A_OR)
	{
		throw_if(!CanAssign(node->left), "argument must be lvalue");
		CheckInt(node->left->type, "left", op.GetValue());
		CheckVoid(node->right->type, "right", op.GetValue());
		CheckConst(node->left->type, op.GetValue());
		node->type = INT;
	}
	else if (op == A_DIV || op == A_MUL || op == A_PLUS || op == A_MINUS)
	{
		throw_if(!CanAssign(node->left), "argument must be lvalue");
		CheckFloat(node->left->type, "left", op.GetValue());
		CheckFloat(node->right->type, "right", op.GetValue());
		CheckConst(node->left->type, op.GetValue());
		node->type = INT;
	}
	else if (op == A_MOD)
	{
		throw_if(!CanAssign(node->left), "argument must be lvalue");
		CheckInt(node->left->type, "left", op.GetValue());
		CheckInt(node->right->type, "right", op.GetValue());
		CheckConst(node->left->type, op.GetValue());
		node->type = INT;
	}
	else if (op == ASSIGN)
	{
		node->left->type->CanConvertTo(node->right->type);
		throw_if(!CanAssign(node->left), "argument must be lvalue");
		throw_if(!node->left->type->CanConvertTo(node->right->type), 
			"Error type: right argument cannot to convert in operation " + op.GetValue());
		CheckConst(node->left->type, op.GetValue());	
		node->type = node->left->type;
	}
	else if (op == POINT || op == ARROW)
	{
		std::string name_s;
		if (op == ARROW)
		{
			throw_if(!isPointer(node->left->type), "Argument must be pointer in operation ->");
			throw_if(!dynamic_cast<SymbolScalar*> (node->left->type->GetBasisType()), "Error type: expect name of struct");
			name_s = dynamic_cast<SymbolScalar*> (node->left->type->GetBasisType())->type;
		} 
		else 
		{
			throw_if(!dynamic_cast<SymbolScalar*> (node->left->type), "Error type: expect name of struct");
			name_s = dynamic_cast<SymbolScalar*> (node->left->type)->type;
		}
		throw_if(name_s.substr(0, 2) != "$_", "Expect struct name");
		std::map<std::string, Symbol*> struct_membes = GetMapOfStruct(name_s, block);
		
		ValNode* member = dynamic_cast<ValNode*>(node->right);
		CallNode* func = dynamic_cast<CallNode*>(node->right);
		ArrNode* arr = dynamic_cast<ArrNode*>(node->right);
		if (func){
			member = dynamic_cast<ValNode*>(func->name);
		}
		else if (arr) {
			member = dynamic_cast<ValNode*>(arr->right);
		}
		throw_if(member->value->GetType() == identifier && !struct_membes.count(member->value->GetValue()),
			"no member " + member->value->GetValue() + " in srtuct " + name_s.substr(2));
		SymbolWithType* symb = dynamic_cast<SymbolWithType*>(struct_membes[member->value->GetValue()]);
		throw_if(member->value->GetType() == identifier && !symb,
			"no member " + member->value->GetValue() + " in srtuct " + name_s.substr(2));
		node->type = member->value->GetType() == identifier ? symb->type : member->type;
	}
	else
		throw_if(true, "Undetected binary operation");
}


void Pars::CheckTypeInArrNode(ArrNode* node, Block* block)
{
	throw_if(!dynamic_cast<SymbolArray*>(node->left->type), 
		"Error type: left argument in operation [] must be array");
	CheckInt(node->right->type, "right", "[]");
	node->type = dynamic_cast<SymbolArray*>(node->left->type)->GetBasisType();
}


void Pars::CheckTypeInCallNode(CallNode* node, Block* block)
{
	ValNode* func = dynamic_cast<ValNode*>(node->name);
	throw_if(!func || func->type != VOID, "Expect function name");
	std::string func_name = func->value->GetValue();
	
	std::vector<SymbolType*> listTypesParams = GetListOfTypesOfParams(func_name, block);
	ExpNode* list = node->arg;
	int len = listTypesParams.size();
	for (int i = 0; i < len - 1; i++)
	{
		BinOpNode* e = dynamic_cast<BinOpNode*>(list);
		throw_if(e && *e->operation != COMMA, "Too few arguments in function '" + func_name + "'" );
		throw_if(!(*listTypesParams[i] == e->left->type), 
			"Error type in function '" + func_name + 
			"', argument numder " + std::to_string(i) +
			" must be " + listTypesParams[i]->toString());
		list = e->left;
	}
	if (len)
	{
		if (dynamic_cast<BinOpNode*>(list))
			throw_if(*dynamic_cast<BinOpNode*>(list)->operation == COMMA, 
				"Too many arguments in function '" + func_name + "'" );
		throw_if(!(*listTypesParams.back() == list->type), 
				"Error type in function '" + func_name + 
				"', argument numder " + std::to_string(listTypesParams.size()) +
				" must be " + listTypesParams.back()->toString());
	}
	node->type = GetTypeFuncByName(block, func_name);
}