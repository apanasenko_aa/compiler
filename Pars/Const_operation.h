#ifndef CONST_OPERATION_H
#define CONST_OPERATION_H
#include <vector>


const int COUNT_LEVELS_BO = 13;
const int COUNT_TOKEN_IN_LEVEL_BO = 10;


const Token* PrecBinOp[COUNT_LEVELS_BO][COUNT_TOKEN_IN_LEVEL_BO] = 
{
/* level 1 */	{new Separator(0, 0, ",", ","), 
					NULL}, 
/* level 2 */  	{new Operation(0, 0, "=", "="),
					new Operation(0, 0, "+=", "+="), 
					new Operation(0, 0, "-=", "-="), 
					new Operation(0, 0, "*=", "*="), 
					new Operation(0, 0, "/=", "/="), 
					new Operation(0, 0, "%=", "%="), 
					new Operation(0, 0, "^=", "^="), 
					new Operation(0, 0, "&=", "&="), 
					new Operation(0, 0, "|=", "|="), 
					NULL},
/* level 3 */	{new Operation(0, 0, "?", "?"),
					NULL},
/* level 4 */	{new Operation(0, 0, "||", "||"),
					NULL},
/* level 5 */	{new Operation(0, 0, "&&", "&&"),
					NULL},
/* level 6 */	{new Operation(0, 0, "|", "|"),  
					NULL},
/* level 7 */	{new Operation(0, 0, "^", "^"),  
					NULL},
/* level 8 */	{new Operation(0, 0, "&", "&"),  
					NULL},
/* level 9 */	{new Operation(0, 0, "==", "=="),
					new Operation(0, 0, "!=", "!="), 
					NULL},
/* level 10 */	{new Operation(0, 0, ">", ">"),
					new Operation(0, 0, "<", "<"),
					new Operation(0, 0, ">=", ">="),
					new Operation(0, 0, "<=", "<="),
					NULL},
/* level 11 */	{new Operation(0, 0, ">>", ">>"), 
					new Operation(0, 0, "<<", "<<"), 
					NULL},
/* level 12 */	{new Operation(0, 0, "+", "+"),
					new Operation(0, 0, "-", "-"),
					NULL},
/* level 13 */	{new Operation(0, 0, "*", "*"),
					new Operation(0, 0, "/", "/"),
					new Operation(0, 0, "%", "%"),
					NULL},
};

const int COUNT_LAO = 9;
const Token* leftAssociativeOp[COUNT_LAO] = 
					{new Operation(0, 0, "=", "="),   
						new Operation(0, 0, "+=", "+="), 
						new Operation(0, 0, "-=", "-="), 
						new Operation(0, 0, "*=", "*="), 
						new Operation(0, 0, "/=", "/="), 
						new Operation(0, 0, "%=", "%="), 
						new Operation(0, 0, "^=", "^="), 
						new Operation(0, 0, "&=", "&="), 
						new Operation(0, 0, "|=", "|="),
					};



const int COUNT_UO = 8;
Token* PrecedencUnOp[COUNT_UO] = 
{
	new Operation(0, 0, "+", "+"),
	new Operation(0, 0, "-", "-"),
	new Operation(0, 0, "*", "*"),
	new Operation(0, 0, "&", "&"),
	new Operation(0, 0, "~", "~"),
	new Operation(0, 0, "!", "!"),
	new Operation(0, 0, "++", "++"),
	new Operation(0, 0, "--", "--"),
};


const Token* COMMA = new Separator(0, 0, ",", ",");
const Token* COLON = new Separator(0, 0, ":", ":");
const Token* SEMICOLON = new Separator(0, 0, ";", ";");
const Token* OPEN_BRACKET  = new Separator(0, 0, "(", "(");
const Token* CLOSE_BRACKET = new Separator(0, 0, ")", ")");
const Token* OPEN_SQ_BRACKET  = new Separator(0, 0, "[", "[");
const Token* CLOSE_SQ_BRACKET = new Separator(0, 0, "]", "]");
const Token* POINT = new Separator(0, 0, ".", ".");
const Token* ARROW = new Operation(0, 0, "->", "->");
const Token* INCREMENT = new Operation(0, 0, "++", "++");
const Token* DECREMENT = new Operation(0, 0, "--", "--");
const Token* QUESTION = new Operation(0, 0, "?", "?");
const Token* FUNC = new Separator(0, 0, "(", "(");
const Token* ARRAY = new Separator(0, 0, "[", "[");
const Token* OPEN_BLOCK = new Separator(0, 0, "{", "{");
const Token* CLOSE_BLOCK = new Separator(0, 0, "}", "}");

const Token* POINTER = new Operation(0, 0, "*", "*");
const Token* ASSIGN = new Operation(0, 0, "=", "=");

const Token* CLASS = new KeyWords(0, 0, "class", "class");
const Token* CONST  = new KeyWords(0, 0, "const", "const");
const Token* STRUCT = new KeyWords(0, 0, "struct", "struct");
const Token* TYPEDEF = new KeyWords(0, 0, "typedef", "typedef"); 
const Token* IF = new KeyWords(0, 0, "if", "if");
const Token* ELSE = new KeyWords(0, 0, "else", "else");
const Token* FOR = new KeyWords(0, 0, "for", "for");
const Token* WHILE = new KeyWords(0, 0, "while", "while");
const Token* DO = new KeyWords(0, 0, "do", "do");
const Token* BREAK = new KeyWords(0, 0, "break", "break");
const Token* CONTINUE = new KeyWords(0, 0, "continue", "continue");
const Token* RETURN = new KeyWords(0, 0, "return", "return");
const Token* PRINTF = new KeyWords(0, 0, "printf", "printf");

const Token* INVERSE = new Operation(0, 0, "~", "~,");
const Token* PLUS = new Operation(0, 0, "+", "+");
const Token* MINUS = new Operation(0, 0, "-", "-");
const Token* AMPERSAND = new Operation(0, 0, "&", "&");
const Token* NEGATIVE = new Operation(0, 0, "!", "!");

const Token* BIT_OR = new Operation(0, 0, "|", "|");
const Token* BIT_AND = new Operation(0, 0, "&", "&");
const Token* BIT_XOR = new Operation(0, 0, "^", "^");
const Token* BIT_MOVE_L = new Operation(0, 0, "<<", "<<");
const Token* BIT_MOVE_R = new Operation(0, 0, ">>", ">>");
const Token* OR = new Operation(0, 0, "||", "||");
const Token* AND = new Operation(0, 0, "&&", "&&");
const Token* EQUAL = new Operation(0, 0, "==", "==");
const Token* NOT_EQUAL = new Operation(0, 0, "!=", "!=");
const Token* LESS = new Operation(0, 0, "<", "<");
const Token* MORE = new Operation(0, 0, ">", ">");
const Token* LESS_E = new Operation(0, 0, "<=", "<=");
const Token* MORE_E = new Operation(0, 0, ">=", ">=");
const Token* MUL= new Operation(0, 0, "*", "*");
const Token* DIV = new Operation(0, 0, "/", "/");
const Token* MOD = new Operation(0, 0, "%", "%");

const Token* A_PLUS = new Operation(0, 0, "+=", "+=");
const Token* A_MINUS = new Operation(0, 0, "-=", "-="); 
const Token* A_MUL = new Operation(0, 0, "*=", "*=");
const Token* A_DIV = new Operation(0, 0, "/=", "/="); 
const Token* A_MOD = new Operation(0, 0, "%=", "%="); 
const Token* A_XOR = new Operation(0, 0, "^=", "^="); 
const Token* A_OR = new Operation(0, 0, "&=", "&=");
const Token* A_AND = new Operation(0, 0, "|=", "|="); 

const ValNode* EMPTY = new ValNode(new Identifier(0, 0, "", ""), NULL);
SymbolScalar* INT = new SymbolScalar("int", true);
SymbolScalar* FLOAT = new SymbolScalar("float", true);
SymbolScalar* CHAR = new SymbolScalar("char", true);
SymbolScalar* VOID = new SymbolScalar("void", true);
SymbolPoint* STRING = new SymbolPoint(new SymbolScalar("char", true));

#endif /* CONST_OPERATION_H */