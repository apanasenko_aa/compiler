#include "CodeGenerator.h"

std::map<asm_register, std::string> AsmArgument::RegToStr;
std::map<asm_command, std::string> AsmInstruction::CommandToStr;	

AsmArgument::AsmArgument() 
{
	RegToStr[EAX] = "EAX";
	RegToStr[EBX] = "EBX";
	RegToStr[ECX] = "ECX";
	RegToStr[EDX] = "EDX";
	RegToStr[EBP] = "EBP";
	RegToStr[ESP] = "ESP";
	RegToStr[CL] = "CL";
	RegToStr[AL] = "AL";
};


AsmInstruction::AsmInstruction()
{
	CommandToStr[cmdDD] = "DD";
	CommandToStr[cmdDB] = "DB";
	CommandToStr[cmdDQ] = "DQ";
	CommandToStr[cmdMOV] = "MOV";
	CommandToStr[cmdPUSH] = "PUSH";
	CommandToStr[cmdPOP] = "POP";
	CommandToStr[cmdMUL] = "MUL";
	CommandToStr[cmdIMUL] = "IMUL";
	CommandToStr[cmdDIV] = "DIV";
	CommandToStr[cmdIDIV] = "IDIV";
	CommandToStr[cmdADD] = "ADD";
	CommandToStr[cmdSUB] = "SUB";
	CommandToStr[cmdINC] = "INC";
	CommandToStr[cmdDEC] = "DEC";
	CommandToStr[cmdRET] = "RET";
	CommandToStr[cmdNEG] = "NEG";
	CommandToStr[cmdCDQ] = "CDQ";
	CommandToStr[cmdINVOKE] = "INVOKE";
	CommandToStr[cmdSHR] = "SHR";
	CommandToStr[cmdSHL] = "SHL";
	CommandToStr[cmdAND] = "AND";
	CommandToStr[cmdOR] = "OR";
	CommandToStr[cmdXOR] = "XOR";
	CommandToStr[cmdNOT] = "NOT";
	CommandToStr[cmdCALL] = "CALL";
	CommandToStr[cmdJMP] = "JMP";
	CommandToStr[cmdCMP] = "CMP";
	CommandToStr[cmdJE] = "JE";
	CommandToStr[cmdJNE] = "JNE";
	CommandToStr[cmdSETG] = "SETG";
	CommandToStr[cmdSETL] = "SETL";
	CommandToStr[cmdSETGE] = "SETGE";
	CommandToStr[cmdSETLE] = "SETLE";
	CommandToStr[cmdSETE] = "SETE";
	CommandToStr[cmdSETNE] = "SETNE";
}


AsmCode& AsmCode::add(AsmCmd* cmd)
{
	commands.push_back(cmd);
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd)
{
	commands.push_back(new AsmCmd(cmd));
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd, AsmArgument* arg)
{
	commands.push_back(new AsmCmd_1(cmd, arg));
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd, AsmArgument* arg1, AsmArgument* arg2)
{
	commands.push_back(new AsmCmd_2(cmd, arg1, arg2));
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd, asm_register reg)
{
	commands.push_back(new AsmCmd_1(cmd, new ArgRegister(reg)));
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd, asm_register reg1, asm_register reg2)
{
	commands.push_back(new AsmCmd_2(cmd, new ArgRegister(reg1), new ArgRegister(reg2)));
	return *this;
}

AsmCode& AsmCode::add(asm_command cmd, asm_register reg, int val)
{
	commands.push_back(new AsmCmd_2(cmd, new ArgRegister(reg), new ArgImmediate(val)));
	return *this;
}

AsmCode& AsmCode::add(ArgLabel* label)
{
	commands.push_back(new InstrLabel(label));
	return *this;
}

AsmCode& AsmCode::add(InOut func, ArgMemory* format, AsmArgument* arg)
{
	commands.push_back(new AsmInOut(func, format, arg));
	return *this;
}


void AsmCode::Print(FILE* f)
{
	std::ofstream fout(f);
	for (int i = 0; i < commands.size(); i++)
		fout << "\t" << commands[i]->generate() << std::endl;
}


std::string AsmCmd::generate() 
{
	return CommandToStr[opCode];
}


std::string InstrLabel::generate()
{ 
	return label->generate() + ":"; 
}


std::string AsmCmd_1::generate()
{
	return CommandToStr[opCode] 
		+ (dynamic_cast<ArgImmediate*>(arg) && opCode != cmdRET ? " dword ptr " : " ") 
		+ arg->generate();
}


std::string AsmCmd_2::generate()
{
	return opCode > cmdDQ 
		 ? CommandToStr[opCode] + " " + arg1->generate() + ", " + arg2->generate() 
		 : arg1->generate() + " " + CommandToStr[opCode] + " " + arg2->generate();
}


std::string AsmInOut::generate()
{
	std::string common = CommandToStr[opCode] + " crt_" 
	                   + (io == Printf ? "printf" : "scanf") 
					   + ", addr " 
					   + format->generate();
	return  common + (arg ? ", " + arg->generate() : "");
}


void CodeGenerator::generate()
{
	std::ofstream fout(file_name);
	if (!fout)
		throw std::string("Cannot open asm file");
	fout << ".686\n"
		 << ".model flat, stdcall\n"
		 << "include " + MASM_Addres + "\\masm32\\include\\msvcrt.inc\n"
		 << "includelib " + MASM_Addres + "\\masm32\\lib\\msvcrt.lib\n"
		 << ".data\n";
	data.Print(file_name);
	fout << ".code\n"
		 << "main:\n"
		 << "\tpush ebp\n"
		 << "\tmov ebp, esp\n";
	
	code.Print(file_name);
	fout << "\t mov esp, ebp\n"
		 << "\t pop ebp\n"	
		 << "\t ret 0\n"
		 << "end main\n"
		 << "start:\n"
		 << "\t call main \n"
		 << "\t ret 0 \n"
		 << "end start \n";
}


ArgImmediate* Immediate(int a) 
{
	return new ArgImmediate(a);
}


ArgMemory* Memory(std::string& varName) 
{
	return new ArgMemory(varName);
}


ArgRegister* Register(asm_register reg) 
{
	return new ArgRegister(reg);
}


ArgIndirect* Indirect(asm_register reg, int offset) 
{
	return new ArgIndirect(reg, offset);
}


ArgString* Stringf(std::string& format)
{
	int pos;
	while((pos = format.find('\n')) != -1)
	{
		format.replace(pos, 1 + (pos == format.size() - 2), pos != format.size() - 2 ? "\", 10, \"" : "\", 10");
	}
	return new ArgString(format + ", 0");
}

ArgDup* Dup(int count)
{
	return new ArgDup(count);
}

//AsmArgLabel* makeLabel(const string& name);
