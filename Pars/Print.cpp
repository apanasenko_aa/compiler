#include "Pars.h"

void UnOpNode::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << operation->GetText() << std::endl;
	value->Print(d + "   ", f);
}


void BinOpNode::Print(std::string d, FILE* f) 
{
	left->Print(d + "   ", f);
	std::ofstream fout(f);
	fout << d << operation->GetText() << std::endl;
	right->Print(d + "   ", f);
}


void TerOpNode::Print(std::string d, FILE* f) 
{
	condition->Print(d + "   ", f);
	std::ofstream fout(f);
	fout << d << "?" << std::endl;
	if_true->Print(d + "   ", f);
	fout << d << ":" << std::endl;
	if_false->Print(d + "   ", f);
}


void ValNode::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << value->GetValue() << std::endl;
}


void CallNode::Print(std::string d, FILE* f) 
{
	name->Print(d + "   ", f);
	std::ofstream fout(f);
	fout << d << "()" << std::endl;
	arg->Print(d + "   ", f);
}


void ArrNode::Print(std::string d, FILE* f) 
{
	left->Print(d + "   ", f);
	std::ofstream fout(f);
	fout << d << "[]" << std::endl;
	right->Print(d + "   ", f);
}


void SymbolScalar::Print(std::string d, FILE* f)
{
	std::ofstream fout(f);
	std::string is_const = isConst ? "const " : "";
	fout << is_const << type;
}
	

void SymbolArray::Print(std::string d, FILE* f)
{
	std::ofstream fout(f);
	fout <<  "[";
	basis_type->Print(d, f);
	fout << " x " << size << "]";
}


void SymbolPoint::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << "*(" ;
	basis_type->Print(d, f);
	fout << ")";	
}


void SymbolValue::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d;
	type->Print(d, f);
	fout << " : " << name << " = ";
	if (value)
	{ 
		fout << std::endl;
		value->Print(d + "\t", f);
	} 
	else 
	{ 
		fout << "NULL" << std::endl;
	}
}


void SymbolFunc::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d;
	type->Print(d, f);
	fout << " : " << name << std::endl;
	fout << d << "Local : " << std::endl;
	if (params) 
		params->Print(d + "\t", f);
	fout << d << "Body  : " << std::endl;
	if (body) 
		body->Print(d + "\t", f);
	else 
		fout << d +"\t" << "Not declared" << std::endl;
}


void Block::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "Symbols :" << std::endl;
	for (int i = 0; i < symbols.size(); i++)
		symbols[i]->Print(d + "\t", f);
	fout << d << "Statements :" << std::endl;
	for (int i = 0; i < statements.size(); i++){
		if (!statements[i]) continue;
		statements[i]->Print(d + "\t", f);
	}
}


void SymbolStruct::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "Struct : " << name << std::endl;
	body->Print(d, f);
}


void SymbolClass::Print(std::string d, FILE* f)
{
	std::ofstream fout(f);
	fout << d << "Class : " << name << (parent ? " (" + parent->name + ") " : " ") << std::endl;
	body->Print(d, f);
}


void SymbolTypedef::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "Typedef : " << name << " = ";
	type->Print(d, f);
	fout << std::endl;
}


void Node_if::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "if :" << std::endl;
	fout << d + "\t" << "condition :" << std::endl;
	if (condition) condition->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "true :" << std::endl;
	if (block_if) block_if->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t"  << "false :" << std::endl;
	if (block_else) block_else->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
}


void Node_for::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "for :" << std::endl;
	fout << d + "\t" << "start :" << std::endl;
	if (start) start->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "end :" << std::endl;
	if (end) end->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "must do :" << std::endl;
	if (must_do) must_do->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "loop :" << std::endl;
	if(block_loop) block_loop->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
}


void Node_while::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "while :" << std::endl;
	fout << d + "\t" << "end :" << std::endl;
	if (end) end->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "loop :" << std::endl;
	if(block_loop) block_loop->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;}


void Node_do::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "do :" << std::endl;
	fout << d + "\t" << "end :" << std::endl;
	if (end) end->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
	fout << d + "\t" << "loop :" << std::endl;
	if(block_loop) block_loop->Print(d + "\t", f);
	else fout << d + "\t" << "nothing" << std::endl;
}


void Node_printf::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "printf :" << std::endl;
	fout << d + "\t" << "format : " << format << std::endl;
	for (int i = 0; i < node.size(); i++)
		node[i]->Print(d + "\t", f);
}


void Node_break::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "break" << std::endl;
}


void Node_continue::Print(std::string d, FILE* f) 
{
	std::ofstream fout(f);
	fout << d << "continue" << std::endl;
}


void Node_return::Print(std::string d, FILE* f)
{
	std::ofstream fout(f);
	fout << d << "return :" << std::endl;
	value->Print(d + "\t", f);
	fout << std::endl;
}


void Node_expression::Print(std::string d, FILE* f) 
{
	exp->Print(d, f);
}


void Node_block::Print(std::string d, FILE* f) 
{
	block->Print(d, f);
}
